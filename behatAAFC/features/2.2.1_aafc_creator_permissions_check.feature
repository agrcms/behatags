Feature: Verfiry that content creation permissions are correct - role Creator

  Scenario: Logged in role Creator should have Add Content options 
   Given I am on "/user/logout"
    Then I pause 2 seconds

   Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | aafc_creator_behat_test |
          | edit-pass | aafc_creator_behat_test_Pass1! |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
  
  # Creator role sees content creation options  
   Given I am on "/node/add"
    Then I should see "Add content" in the "h1" element
    And I should see "Internal page"
    And I should see "Landing page"
    But I should not see "Directory listing"
    And I should not see "Webform"

  # Creator role sees Content administration page  
   Given I am on "/admin/content"
    Then I pause 2 seconds
    Then I should see "Content" in the "h1" element
    And I should see "Displaying 1 - "

  # Creator role sees Find Replace Scanner administration page  
   Given I am on "/admin/content/scanner"
    Then I pause 2 seconds
    Then I should see "Search and Replace Scanner" in the "h1" element
    And I should see "Step 1: Search for"

  # Creator role sees Search URL administration page  
   Given I am on "/admin/search-page-url"
    Then I pause 2 seconds
    Then I should see "Search by Page URL" in the "h1" element
    And I should see "Node ID"

  # Creator role sees Safe Delete Report administration page  
   Given I am on "/admin/content/safedelete-orphanedpages/viewreport"
    Then I pause 2 seconds
    Then I should see "View Orphaned Nodes Report" in the "h1" element
    And I should see "Node ID"

  # Creator role sees Publishing Activity administration page  
   Given I am on "/admin/publish_activity_new"
    Then I pause 2 seconds
    Then I should see "Publish Activity of New Pages" in the "h1" element
    And I should see "Date First Published between"

  # Creator role sees Block administration page  
   Given I am on "/admin/content/block"
    Then I pause 2 seconds
    Then I should see "Custom block library" in the "h1" element
    And I should see "Block description"

  # Creator role sees Menu administration page  
   Given I am on "/admin/structure/menu"
    Then I pause 2 seconds
    Then I should see "Menus" in the "h1" element
    And I should see "Operations"

  # Creator role sees Taxonomy administration page  
   Given I am on "/admin/structure/taxonomy"
    Then I pause 2 seconds
    Then I should see "Taxonomy" in the "h1" element
    And I should see "Vocabulary name"

  # Creator role sees Webform administration page  
   Given I am on "/admin/structure/webform"
    Then I pause 2 seconds
    Then I should see "Webforms" in the "h1" element
    And I should see "Filter webforms"

  # Creator role sees Link Checker administration page  
   Given I am on "/admin/reports/linkchecker"
    Then I pause 2 seconds
    Then I should see "Broken link report" in the "h1" element
    And I should see "Last checked"
