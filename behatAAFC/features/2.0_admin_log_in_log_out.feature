Feature: Test the log in functionality

  Scenario: Verify that admin user is able to login
  # Force logout to ensure a valid test run
   Given I am on "/user/logout"
    And I pause 3 seconds
    
  # Admin log in testing
   Given I am on "/user/login"
    Then I should see "Log in" in the "h1" element
    And I should not see "Error message"
    Then I fill in the following:
          | edit-name | aafcbehat_adm |
          | edit-pass | AdminPasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    And I pause 4 seconds
    Then I should not see "Unrecognized username or password"
    And I should see "Moderation Dashboard" in the "h1" element

  # Admin log out testing
   Given I am on "/user/logout"
    And I pause 3 seconds
    Then I should see "Agriculture and Agri-Food Canada" in the "h1" element
