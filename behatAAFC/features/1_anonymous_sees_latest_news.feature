Feature: Presence of Latest News block

  Scenario: Verify that Search for more Good News Grows stories is in place
    Given I navigate to "/en/news-agriculture-and-agri-food-canada/news-releases-and-statements"
    Then I should see "News releases and statements" in the "h1#wb-cont" element
    And I should see "Latest" in the "h2.panel-title" element
    And I should see a "small" element
    And I should see a "time" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Communiqués et déclarations" in the "h1#wb-cont" element
    And I should see "Nouveautés" in the "h2.panel-title" element
    And I should see a "small" element
    And I should see a "time" element
    And I should not see "Message d'erreur"
