Feature: Clean up the leftover users created during earlier behat test scenarios

Scenario: All users created during a full run of behat tests should be deleted.

    # First logout of the system, then login using admin level account
   Given I am on "/user/logout"
    And I pause 3 seconds
    Given I am on "/user/login"
    Then I should see "Log in"
    And I should not see "Error message"
    Then I fill in the following:
          | edit-name | aafcbehat_adm |
          | edit-pass | AdminPasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    And I pause 4 seconds
    Then I should not see "Unrecognized username or password"
    And I should see "Moderation Dashboard"

    # Delete User(s) and clean up the content created during previous tests
    Given I am on "/admin/people/behat"
    And I pause 2 seconds
    Then I should see "People" in the "h1" element
    And I fill in "#edit-user" with "aafc_creator_behat_test"
    And I click the "#edit-submit-user-admin-people" button
    And I pause 5 seconds
    Then I should see "aafc_creator_behat_test edit link"
    When I click "aafc_creator_behat_test edit link"
    And I pause 5 seconds
    Then I click the "#edit-delete" button
    And I pause 3 seconds
    And I click the "input" element with "value" attribute containing "user_cancel_delete"
    Then I click the "#edit-submit" button
    And I pause 12 seconds
    Then I should see "Account aafc_creator_behat_test has been deleted"
 
    # Final logout of the system
    Given I am on "/user/logout"
    And I pause 4 seconds
    
