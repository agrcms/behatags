Feature: Report a problem on this page is no longer in place

  Scenario: Verify that Report a problem on this page is no longer in place
    Given I navigate to "/en/"
    Then I should see "Agriculture and Agri-Food Canada" in the "h1#wb-cont" element
    And I should not see "Report a problem on this page"
    # in the "section#block-aafctheme-reportproblemblock" element
    #When I click the "section#block-aafctheme-reportproblemblock" button
    #Then I should see "It has a spelling mistake"

    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Agriculture et Agroalimentaire Canada" in the "h1#wb-cont" element
    And I should not see "Signaler un problème sur cette page"
    # in the "section#block-aafctheme-reportproblemblock" element
    #When I click the "section#block-aafctheme-reportproblemblock" button
    #Then I should see "Il y a une erreur d'orthographe ou de grammaire"
