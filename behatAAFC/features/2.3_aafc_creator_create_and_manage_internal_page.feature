Feature: Create new Internal Page content and publish content - role Creator

  Scenario: Logged in users should be able to create and manage content appropriate to their roles - role Creator
   Given I am on "/user/logout"
    Then I pause 2 seconds

   Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | aafc_creator_behat_test |
          | edit-pass | aafc_creator_behat_test_Pass1! |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
  
  # Creator role creates a new Internal Page  
    # Verify that we are on the Create Internal Page form and that an empty form submission generates validation errors
   Given I am on "/node/add/page"
    Then I should see "Create Internal page" in the "h1" element
    When I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "Title (English) field is required."
    And I should see "Title (French) field is required."

    # Add input or make selections for each field
      # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in "edit-title-0-value" with "Test Internal Page title EN by aafc_creator_behat_test"   
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field
    And I fill in "edit-title-etuf-fr-0-value" with "Test Internal Page title FR by aafc_creator_behat_test"
    
    Then I fill in the following:
      | edit-revision-log-0-value | revision log Internal Page test by agrisourcecreator@yopmail.com |
      | edit-field-tracking-number-0-value | Test-tracking-number-1234 |

    # Verify functionality of the button ... Preview
    When I click the "#edit-preview" button
    And I pause 5 seconds
    Then I should see "Back to content editing"
    Then I should see "Test Internal Page title EN by aafc_creator_behat_test" in the "h1" element
    And I should not see "Error message"
    
    # Verify functionality of the link ... Return to form in progress
    When I click the "#edit-backlink" button
    And I pause 5 seconds
    Then I should not see "Back to content editing"

    # Verify functionality of template option
    Then I force check "Special Title"
    And I pause 1 seconds
    Then I click on the element with css selector ".menu-link-form.accordion__item"
    And I pause 1 seconds
    Then I force check "Provide a menu link"

    # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 5 seconds

    # Successful Submit should display a Confirmation message and now Viewing the Landing Page
    Then I should see "has been created."
    And I should not see "Undefined variable"
    Given I am on "/test-internal-page-title-aafccreatorbehattest"
    Then I should see "sentences that describe the topics and top tasks"
    # And I should see "Winterlude has finally arrived!"

    # Verify breadcrumbs are correct
    And I should see "Canada.ca" in the "nav#wb-bc" element
    And I should see "Agriculture and Agri-Food Canada" in the "nav#wb-bc" element
 
    # Verify that Access Unpublished is confirmed as an option for this Draft version
    And I should see "Access unpublished hash link allows unpublished access to content." in the "div.alert-success" element

    # Verify that the language switcher works to FR
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Test Internal Page title FR by aafc_creator_behat_test" in the "h1" element

    # Verify breadcrumbs are correct
    And I should see "Canada.ca" in the "nav#wb-bc" element
    And I should see "Agriculture et Agroalimentaire Canada" in the "nav#wb-bc" element
    And I should not see "Message d'erreur"
    And I should see "1 ou 2 phrases d’introduction qui"

    # Test workflow moderation
   Given I am on "/admin/content/behat"
    And I pause 2 seconds
    Then I click "Test Internal Page title EN by aafc_creator_behat_test edit link"
    And I pause 2 seconds
    Then I should not see "This content is being edited by the user"

    # Test workflow fro Draft to Published step
    When I select option "Published" from "edit-moderation-state-0-state"
    And I click the "#edit-submit" button
    And I pause 2 seconds
    Then I click the "button.button--primary" button
    And I pause 2 seconds
    Then I should see "has been updated."

    # Test workflow from Published to Archived step
    Then I click "Test Internal Page title EN by aafc_creator_behat_test edit link"
    And I pause 2 seconds
    Then I should not see "This content is being edited by the user"
    When I select option "Archived" from "edit-moderation-state-0-state"
    And I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
