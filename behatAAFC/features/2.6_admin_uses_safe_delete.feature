Feature: Test the safe delete functionality

  Scenario: Verify that admin user is able to user safe delete
  # Login the admin account 
   Given I am on "/user/logout"
    And I pause 3 seconds
    Given I am on "/user/login"
    Then I should see "Log in"
    And I should not see "Error message"
    Then I fill in the following:
          | edit-name | aafcbehat_adm |
          | edit-pass | AdminPasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    And I pause 4 seconds
    Then I should not see "Unrecognized username or password"
    And I should see "Moderation Dashboard"

    # Test Internal Page safe delete
    Given I am on "/admin/content/behat"
    And I pause 5 seconds
    Then I click "Test Internal Page title EN by aafc_creator_behat_test edit link"
    And I pause 5 seconds
    Then I should not see "This content is being edited by the user"
    When I click the "#edit-delete" button
    And I pause 4 seconds
    Then I should see "Internal page"
    # Latest version of safedelete could use some performance profiling.
    And I pause 20 seconds
    Then I should see "Are you sure you want to delete the content item" in the "h1" element
    Then I should see "Safe Delete verification check status"
    When I click the "#edit-submit" button
    And I pause 12 seconds
    Then I should see "has been deleted."
    
    # Test Landing Page safe delete
    Given I am on "/admin/content/behat"
    And I pause 5 seconds
    Then I click "Test Landing Page title EN by aafc_creator_behat_test edit link"
    And I pause 5 seconds
    Then I should not see "This content is being edited by the user"
    When I click the "#edit-delete" button
    And I pause 4 seconds
    Then I should see "Landing page"
    # Latest version of safedelete could use some performance profiling.
    And I pause 20 seconds
    Then I should see "Are you sure you want to delete the content item" in the "h1" element
    Then I should see "Safe Delete verification check status"
    When I click the "#edit-submit" button
    And I pause 12 seconds
    Then I should see "has been deleted."
