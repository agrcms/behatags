Feature: Anonymous sees Homepage

  Scenario: Anonymous user must see the Homepage correctly in both EN and FR
  # Verify that on the bilingual splash page I should see certain elements.
    Given I navigate to "/"
    And I should see "Terms and conditions"

  # Now at the front homepage we expect to see these elements
    Given I navigate to "/en"
    Then I should see "Agriculture and Agri-Food Canada" in the "#wb-cont" element
    And I should see "Date modified" in the "#block-aafctheme-datemodifiedblock" element
    And I should see "Terms and conditions"
    And I should see "agriculture.canada.ca" in the "#searchoptions" element
    And I should see "canada.ca" in the "#searchoptions" element
    And I should not see "Error message"

  # Verify language switcher leads from EN to FR homepage
    Then I pause 1 seconds
    Then I click the "a.language-link" button
    Then I should see "English"

  # Verify that on the FR homepage we do see the expected elements
    Then I should see "Agriculture et Agroalimentaire Canada" in the "#wb-cont" element
    And I should see "Date de modification" in the "#block-aafctheme-datemodifiedblock" element
    And I should see "Avis"
    And I should not see "Message d'erreur"

  # Verify language switcher leads from FR back to EN homepage
    Then I should see "English"
    Then I click the "a.language-link" button
    Then I should see "Français"
    Then I should see "Agriculture and Agri-Food Canada" in the "#wb-cont" element
    And I should see "Date modified" in the "#block-aafctheme-datemodifiedblock" element


