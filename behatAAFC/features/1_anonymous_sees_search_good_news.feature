Feature: Presence of search - Search for more Good News Grows stories

  Scenario: Verify that Search for more Good News Grows stories is in place
    Given I navigate to "/en/agri-info/good-news-grows"
    Then I should see "Search for more Good News Grows stories" in the "h2#search" element
    And I should see "Showing 1 to 10 of" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Chercher d'autres histoires dans Les bonnes nouvelles sont dans le pré" in the "h2#search" element
    And I should see "Affiche 1 à 10 de" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
