Feature: Create new Landing Page content and publish content - role Creator

  Scenario: Logged in users should be able to create and manage content appropriate to their roles - role Creator
    Given I am on "/user/logout"
    Then I pause 2 seconds

   Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | aafc_creator_behat_test |
          | edit-pass | aafc_creator_behat_test_Pass1! |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
  
  # Creator role creates a new Landing Page  
    # Verify that we are on the Create Landing Page form and that an empty form submission generates validation errors
    Given I am on "/node/add/landing_page"
    Then I should see "Create Landing Page" in the "h1" element
    When I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "Title (English) field is required."
    And I should see "Title (French) field is required."

    # Add input or make selections for each field
        # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
   # Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in "edit-title-0-value" with "Test Landing Page title EN by aafc_creator_behat_test"   
   # Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field
    And I fill in "edit-title-etuf-fr-0-value" with "Test Landing Page title FR by aafc_creator_behat_test"
    
    Then I fill in the following:
      | edit-revision-log-0-value | revision log Landing Page test by agrisourcecreator@yopmail.com |
      | edit-field-tracking-number-0-value | Test-tracking-number-1234 |

    # Test the application of the first content template
    Then I force check "Special Title"
    And I pause 1 seconds

    # Test the enabling of the Menu link option
    Then I click on the element with css selector ".menu-link-form.accordion__item"
    And I pause 1 seconds
    Then I force check "Provide a menu link"

    # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 5 seconds

    # Successful Submit should display a Confirmation message and now Viewing the Landing Page
    Then I should see "has been created."
    And I should not see "Undefined variable"

    # Confirm the EN application of the first content template
    Then I should see "sentences that describe the topics and top tasks"

    # Verify breadcrumbs are correct
    And I should see "Canada.ca" in the "nav#wb-bc" element
    And I should see "Agriculture and Agri-Food Canada" in the "nav#wb-bc" element

    # Verify that the language switcher works to FR and back to EN and also that breadcrumbs are correct in both languages
    Then I should see "Test Landing Page title EN by aafc_creator_behat_test" in the "h1" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Test Landing Page title FR by aafc_creator_behat_test" in the "h1" element

    # Verify breadcrumbs are correct
    And I should see "Canada.ca" in the "nav#wb-bc" element
    And I should see "Agriculture et Agroalimentaire Canada" in the "nav#wb-bc" element

    # Confirm the FR application of the first content template
    And I should see "1 ou 2 phrases d’introduction qui"

    # Test the second template
   Given I am on "/admin/content/behat"
    And I pause 2 seconds
    Then I click "Test Landing Page title EN by aafc_creator_behat_test edit link"
    And I pause 5 seconds
    Then I should not see "This content is being edited by the user"
    And I should see "Edit Landing page" in the "h1" element

    # Test the application of the second content template
    When I uncheck "Special Title"
    And I pause 5 seconds
    Then I force check "Campaign"
    And I pause 5 seconds

    # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
    Then I click "Test Landing Page title EN by aafc_creator_behat_test"
    And I pause 5 seconds
    And I should see "Test Landing Page title EN by aafc_creator_behat_test" in the "h1" element

    # Confirm the EN application of the first content template
    Then I should see "Winterlude"

    # Verify that the language switcher works to FR
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Test Landing Page title FR by aafc_creator_behat_test" in the "h1" element

    # Confirm the FR application of the first content template
    And I should see "Bal de Neige"

    # Test workflow moderation
   Given I am on "/admin/content/behat"
    And I pause 2 seconds
    Then I click "Test Landing Page title EN by aafc_creator_behat_test edit link"
    And I pause 2 seconds
    Then I should not see "This content is being edited by the user"

    # Test workflow fro Draft to Published step
    When I select option "Published" from "edit-moderation-state-0-state"
    And I click the "#edit-submit" button
    And I pause 2 seconds
    Then I click the "button.button--primary" button
    And I pause 2 seconds
    Then I should see "has been updated."

    # Test workflow from Published to Archived step
    Then I click "Test Landing Page title EN by aafc_creator_behat_test edit link"
    And I pause 2 seconds
    Then I should not see "This content is being edited by the user"
    When I select option "Archived" from "edit-moderation-state-0-state"
    And I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
