Feature: Test the content locking and unlocking functionality
  # Test preparation - create a content page that Creator does not own
  Scenario: Admin user should create a page for testing of Lock/Unlock functionality
   Given I am on "/user/logout"
    Then I pause 2 seconds

   Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | aafcbehat_adm |
          | edit-pass | AdminPasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
  
   Given I am on "/node/add/page"
    Then I should see "Create Internal page" in the "h1" element
    When I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "Title (English) field is required."
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in "edit-title-0-value" with "Lock Test Internal Page title EN by admin"   
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field
    And I fill in "edit-title-etuf-fr-0-value" with "Lock Test Internal Page title FR by admin"
    When I click the "#edit-submit" button
    And I pause 5 seconds

    # Successful Submit should display a Confirmation message and now Viewing the Internal Page
    Then I should see "has been created."
    And I should see "Lock Test Internal Page title EN by admin" in the "h1" element

  Scenario: Logged in users should lock content when editing and also unlock when saved or manually unlocked
    # First logout then log in with aafc_creator 
    Given I am on "/user/logout"
    Then I pause 4 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | aafc_creator_behat_test |
          | edit-pass | aafc_creator_behat_test_Pass1! |
    And I click the "#edit-submit" button
    Then I pause 12 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
    
    # Verify that we are on the Content admin page
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Lock Test Internal Page title EN by admin"
    And I click the "#edit-submit-content" button
    And I pause 3 seconds
    Then I should see "Lock Test Internal Page title EN by admin edit link"
    When I click "Lock Test Internal Page title EN by admin edit link"
    And I pause 6 seconds

    # Test that saving content from Draft to Published does unlock the content
    Then I should see "This content is now locked against simultaneous editing"

    And I should see "Draft" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "published" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."

   # Return to /behat view to confirm content is now unlocked
    Given I am on "/admin/content/behat"
    And I pause 3 seconds
    Then I should see "Lock Test Internal Page title EN by admin edit link"
    And I should not see "Locked by agrisourcebehat_cspub"
    
    # Return to edit to once more lock the content and test the Unlock button without saving
    When I click "Lock Test Internal Page title EN by admin edit link"
    And I pause 1 seconds
    Then I should see "This content is now locked against simultaneous editing"
    And I should see "Published" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I click the "#edit-unlock" button
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "Lock broken. Anyone can now edit this content."
    And I should not see "Error message"

   # Return to /behat view to confirm content is now unlocked
    Given I am on "/admin/content/behat"
    And I pause 3 seconds
    Then I should see "Lock Test Internal Page title EN by admin edit link"
    When I click "Lock Test Internal Page title EN by admin edit link"
    And I pause 1 seconds
    Then I should see "This content is now locked against simultaneous editing"

   # Set the page to Archived and Lock
    And I should see "Published" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm publish dialog
    And I pause 1 seconds
    Then I select option "archived" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
    
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    When I click "Lock Test Internal Page title EN by admin edit link"
    And I pause 1 seconds
    Then I should see "This content is now locked against simultaneous editing"

   # Set the page to Draft
    And I should see "Archived" in the "#edit-moderation-state-0-current" element
    Then I ask behat to disable the confirm revive alert
    And I pause 1 seconds
    Then I select option "draft" from "edit-moderation-state-0-state"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "has been updated."
    And I should not see "Error message"
