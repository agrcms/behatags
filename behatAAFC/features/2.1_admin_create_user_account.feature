Feature: Test the admin account creation functionality

  Scenario: Verify that admin user is able to create user accounts
  # Login the admin account in preparation for creating new account
   Given I am on "/user/logout"
    And I pause 3 seconds
    Given I am on "/user/login"
    Then I should see "Log in"
    And I should not see "Error message"
    Then I fill in the following:
          | edit-name | aafcbehat_adm |
          | edit-pass | AdminPasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    And I pause 4 seconds
    Then I should not see "Unrecognized username or password"
    And I should see "Moderation Dashboard"

    # Create a new account with role Creator
   Given I am on "/en/admin/people/create"
    And I pause 13 seconds
    Then I should see "Add user" in the "h1" element
    When I fill in the following:
          | edit-name | aafc_creator_behat_test |
          | edit-pass-pass1 | aafc_creator_behat_test_Pass1! |
          | edit-pass-pass2 | aafc_creator_behat_test_Pass1! |
    Then I should see "yes"
    And I force check "Creator"
    And I click the "input" element with "value" attribute containing "Create new account"
    And I pause 6 seconds

    # Confirm the new account with role Creator has been created successfully
    Then I should see "Created a new user account for "
    
    # Click the link in Success box and confirm dashboard My Account is loading
    When I click "aafc_creator_behat_test"
    And I pause 6 seconds
    Then I should see "My account" in the "h1" element
