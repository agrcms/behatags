Feature: Date Modified Presence

  Scenario Outline: Verify that Date Modified block appears in many places
    Given I navigate to "<url>"
    Then I should see "<page-text-en>"
    And I should not see "Error message"
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "<page-text-fr>" in the "section#block-aafctheme-datemodifiedblock" element
    And I should not see "Message d'erreur"

    Examples:
      | url | page-text-en |page-text-fr |
      | /en | Date modified | Date de modification |
      | /agricultural-programs-and-services | Date modified | Date de modification |
      | /canadas-agriculture-sectors | Date modified | Date de modification |
      | /agriculture-and-environment | Date modified | Date de modification |
      | /about-our-department/key-departmental-initiatives/food-policy | Date modified | Date de modification |
      | /agricultural-science-and-innovation | Date modified | Date de modification |
      | /international-trade | Date modified | Date de modification |
      | /youth-agriculture | Date modified | Date de modification | 
      | /indigenous-peoples-canadian-agriculture | Date modified | Date de modification |
