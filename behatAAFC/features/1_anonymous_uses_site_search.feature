Feature: Presence and basic funtionality of main site search

  Scenario: Verify that the main Site Search is in place and working
    Given I navigate to "/en"
    Then I should see "Agriculture and Agri-Food Canada" in the "h1#wb-cont" element
    And I should see "agriculture.canada.ca" in the "#searchoptions" element
    And I should not see "Error message"
    When I fill in "#q" with "food"
    And I select option "agriculture.canada.ca" from "searchoptions"
	And I click the "#wb-srch-sub" button
    And I pause 5 seconds
    Then I should see "Search Agriculture and Agri-Food Canada" in the "h1#wb-cont" element
    Given I navigate to "/en"
    And I pause 2 seconds
    When I fill in "#q" with "food"
    And I select option "canada.ca" from "searchoptions"
	And I click the "#wb-srch-sub" button
    And I pause 5 seconds
    Then I should see "Search results" in the "h1#wb-cont" element
#    When I run the test in French
    Given I navigate to "/fr"
    And I pause 2 seconds
    Then I should see "Agriculture et Agroalimentaire Canada" in the "h1#wb-cont" element
    And I should see "agriculture.canada.ca" in the "#searchoptions" element
    And I should not see "Message d'erreur"
    When I fill in "#q" with "food"
    And I select option "agriculture.canada.ca" from "searchoptions"
	And I click the "#wb-srch-sub" button
    And I pause 5 seconds
    Then I should see "Rechercher Agriculture et Agroalimentaire Canada" in the "h1#wb-cont" element
    Given I navigate to "/fr"
    And I pause 2 seconds
    When I fill in "#q" with "food"
    And I select option "canada.ca" from "searchoptions"
	And I click the "#wb-srch-sub" button
    And I pause 5 seconds
    Then I should see "Résultats de la recherche" in the "h1#wb-cont" element
