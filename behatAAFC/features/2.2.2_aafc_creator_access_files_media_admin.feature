Feature: Verify that Creator role can access Files admin and manage files

  Scenario: role Creator administers Files
    Given I am on "/user/logout"
    Then I pause 2 seconds

    Given I am on "/user/login"
      Then I should see "Log in"
      Then I fill in the following:
            | edit-name | aafc_creator_behat_test |
            | edit-pass | aafc_creator_behat_test_Pass1! |
      And I click the "#edit-submit" button
      Then I pause 2 seconds
      And I should see "Moderation Dashboard" in the "h1" element
      And I should not see "Error message"
    
    # Creator role sees Files admin page
    Given I am on "/admin/content/files"
      Then I should see "Files" in the "h1" element
      And I should see "Displaying 1 - "

    # Creator role sees Media administration page  
    Given I am on "/admin/content/media-grid"
      Then I pause 2 seconds
      Then I should see "Media" in the "h1" element
      And I should see "Select all media"

    # Confirm Creator can access Bulk Upload
      When I click "Bulk upload"
      And I pause 2 seconds
      Then I should see "Bulk upload" in the "h1" element
      And I should see "Drag files here to upload them"

      When I go back
      And I pause 2 seconds
      Then I should see "Media" in the "h1" element

    # Confirm Creator can access Table view
      When I click "Table"
      And I pause 2 seconds
      Then I should see "Media entities" in the "h1" element
      And I should see "Thumbnail"

    # Creator role sees the correct Add Media options
    Given I am on "/media/add"
      And I pause 2 seconds
      Then I should see "Add media item" in the "h1" element
      And I should see "Audio file"
      And I should see "Document"
      And I should see "Image"
      And I should see "Instagram"
      And I should see "Instagram"
      And I should see "Instagram"
      And I should see "Instagram"
      And I should see "Video"
      And I should see "Video file"
