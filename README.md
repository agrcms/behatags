Agriculture intranet Portal CI PIPELINE STATUS IS BELOW! https://gitlab.com/agrcms/behatags/pipelines

Agriculture intranet portal

[![pipeline status](https://gitlab.com/agrcms/behatags/badges/master/pipeline.svg)](https://gitlab.com/agrcms/behatags/commits/master)

*MORE DOCUMENTATION*

For usage documentation see readme.md and /or the wiki here h t t p s: / / g i t l a b . w 3 u s i n e . c o m / q a / b e h a t Example/wikis/create-a-new-automated-test-scenario-and-run-it

*Reference documentation for behat*

Refer to this reference document for how to write a test: https://gitlab.com/agrcms/behatags/-/blob/master/behatAgrisource/features_examples/behat-steps-examples.feature.temp

BACKSTOPJS
the folder called backstopjs_ags contains configuration for backstopjs to run DWP tests, as of this writing,
the entrypoint on the docker container is incorrect so a slight tweak is needed to get that running. backstopjs might also work.
This is a very basic example configuration for using backstopjs.  I (Joseph) will followup on this asap.


Agrisource behat test coverage as of June 2023:

- Anonymous sees news at work bulletin and other elements of news at work verified
- Send email
- Security updates are applied
- anonymous sees date modified on 20+ pages
- anonymous sees homepage
- anonymous submits employment opportunity, workflow and validation and labels are verified as well as translations, submits draft empl EN/FR
- anonymous submits news article, workflow and validation and labels are verified as well as translations, submits draft news EN/FR
- Anonymous submits publishing creative services (similar validations as above in EN/FR) , tests for successful submit
- Embed block functionality is tested/working
- Add users (admin behat adds creator , cspub, )
- cs pub tests lock and unlocking functionality
- creator create and manage internal page (workflow and make draft/publish/archive this content)
- creator create and manage landing page (^^^^) EN/FR for both ^^
- ccpub manage news article ( workflow publishing/archive/draft) both EN/FR
- cspub manage employment opportunities (publish draft, archive and to draft) both EN/FR
- clean up content, delete all automated test created content
- clean up published check url logic
- clean up (delete automatically created users)
