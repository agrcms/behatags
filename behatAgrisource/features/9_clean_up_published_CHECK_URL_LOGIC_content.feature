Feature: Clean up the leftover CHECK URL LOGIC content created during earlier behat test scenarios

Scenario: All CHECK URL LOGIC content created during a full run of behat tests should be deleted.

    # First logout of the system, then login using admin level account
    Given I am on "/user/logout"
    And I pause 4 seconds
#    Then I should see "Find a colleague"

    Given I am on "/user/login"
    Then I should see "Log in"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button
    Then I should see "Moderation Dashboard" in the "h1" element
    And I should not see "Error message"
    
    # Delete (s)
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Test CHECK URL LOGIC Landing page title EN by agrisourcebehat_creator"
    And I click the "#edit-submit-content" button
    And I pause 5 seconds
    Then I should see "Test CHECK URL LOGIC Landing page title EN by agrisourcebehat_creator delete link"
    When I click "Test CHECK URL LOGIC Landing page title EN by agrisourcebehat_creator delete link"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    Then I should see "Test CHECK URL LOGIC Landing page title EN by agrisourcebehat_creator has been deleted"

    # Delete (s)
    Given I am on "/admin/content/behat"
    And I pause 2 seconds
    And I fill in "edit-title" with "Test Landing page title EN by agrisourcebehat_creator"
    And I click the "#edit-submit-content" button
    And I pause 5 seconds
    Then I should see "Test Landing page title EN by agrisourcebehat_creator delete link"
    When I click "Test Landing page title EN by agrisourcebehat_creator delete link"
    And I pause 5 seconds
    Then I click the "#edit-submit" button
    Then I should see "Test Landing page title EN by agrisourcebehat_creator has been deleted"
    
