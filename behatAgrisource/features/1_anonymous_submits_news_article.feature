Feature: Anonymous submits a News Article

  Scenario: Anonymous user must be able to submit a News Article in both EN and FR and the form validation should work on each field.
  # Verify that we are on the ENGLISH News Article submit form and that an empty form submission generates validation errors
    Given I am on "/news/newswork/submit-article-news-work"
    Then I should see "Submit an article for news@work" in the "h1#wb-cont" element
    When I click the "button#edit-submit" button
    And I pause 1 seconds
    Then I should see "Communications Advisor who reviewed this article (agr.gc.ca email address) field is required."
    And I should see "Director or above who approved this article (agr.gc.ca email address) field is required."
    And I should see "Category field is required."
    And I should see "To: field is required."
    And I should see "Headline (English) field is required."
    And I should see "Headline (French) field is required."
    And I should see "Sent by: (English) field is required."
    And I should see "Sent by: (French) field is required."
    And I should see "Article text (English) field is required."
    And I should see "Article text (French) field is required."
    And I should see "Translation request number field is required."
    And I should see "Date field is required."
    And I should see "Name field is required."
    And I should see "Email address (agr.gc.ca) field is required."
    And I should see "Telephone number field is required."
    And I should not see "Error message"

    # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field

  # Add input or make selections for each mandatory field
    And I fill in the following:
      | edit-field-newsreviewedby-0-value | agsbehat-news-tester@yopmail.com |
      | edit-field-newsapprovedby-0-value | agsbehat-news-tester@yopmail.com |
    When I select option "General" from "edit-field-newstype"
    When I select option "All Staff" from "edit-field-to"
    Then I fill in the following:
      | edit-title-0-value | Test Headline EN (EN form) |
      | edit-title-etuf-fr-0-value | Test Headline FR (EN form) |
      | edit-field-from-0-value | agsbehat-news-tester@yopmail.com |
      | edit-field-from-etuf-fr-0-value | agsbehat-news-tester@yopmail.com |
    And I fill in wysiwyg on field "edit-body-0-value" with "Test EN article text. (EN form)"
    And I fill in wysiwyg on field "edit-body-etuf-fr-0-value" with "Test FR article text. (EN form)"
    Then I fill in the following:
      | edit-field-newstranslationnum-0-value | 123456 |
      | edit-field-posted-0-value-date | 06/09/2020 |
      | edit-field-newssubmittername-0-value | Test News Submitter Name (EN form) |
      | edit-field-newssubmitteremail-0-value | agsbehat-news-tester@yopmail.com |
      | edit-field-newssubmitterphone-0-value | Test News Submitter Phone 12345 (EN form)|

  # Verify each validation error is now gone
    When I pause 1 seconds
    Then I should not see "Communications Advisor who reviewed this article (agr.gc.ca email address) field is required."
    Then I should not see "Director or above who approved this article (agr.gc.ca email address) field is required."
    Then I should not see "Category field is required."
    Then I should not see "To: field is required."
    Then I should not see "Headline (English) field is required."
    Then I should not see "Headline (French) field is required."
    Then I should not see "Sent by: (English) field is required."
    Then I should not see "Sent by: (French) field is required."
#    Then I should not see "Article text (English) field is required."
#    Then I should not see "Article text (French) field is required."
    Then I should not see "Translation request number field is required."
    Then I should not see "Date field is required."
    Then I should not see "Name field is required."
    Then I should not see "Email address (agr.gc.ca) field is required."
    Then I should not see "Telephone number field is required."
    And I should not see "Error message"

  # TODO - Only mandatory fields are validated, non-mandatory fields and not tested currently
    # Add Media
    # Notes

  # Verify functionality of the button ... Preview
    When I click the "#edit-preview" button
    And I pause 10 seconds
    Then I should see "Back to content editing"
    And I should see "Test Headline EN (EN form)"

  # Verify functionality of the link ... Return to form in progress
    When I click the "#edit-backlink" button
    And I pause 10 seconds
    Then I should not see "Back to content editing"

  # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 10 seconds

  # Successful Submit should display a Confirmation message
    And I should see "has been created."
  # Successful Submit should auto-forward to the Home Page
    Then I should see "Find a colleague"

  # Verify that the language switcher works from EN News submit form to FR and back
    Given I am on "/news/newswork/submit-article-news-work"
    Then I should see "Submit an article for news@work" in the "h1#wb-cont" element
    Then I should see "Home" in the "nav#wb-bc" element
    And I should see "News" in the "nav#wb-bc" element
    And I should see "news@work" in the "nav#wb-bc" element
    And I should see "Submit an article for news@work" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Soumission d'un article pour nouvelles@l'ouvrage" in the "h1#wb-cont" element
    Then I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Nouvelles" in the "nav#wb-bc" element
    And I should see "nouvelles@l'ouvrage" in the "nav#wb-bc" element
    And I should see "Soumission d'un article pour nouvelles@l'ouvrage" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Submit an article for news@work" in the "h1#wb-cont" element

  # Verify that we are on the FRENCH News Article submit form and that an empty form submission generates validation errors
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Soumission d'un article pour nouvelles@l'ouvrage" in the "h1#wb-cont" element
    When I click the "button#edit-submit" button
    And I pause 2 seconds
    Then I should see "Le champ Conseiller/e en communications qui a revu l’article (adresse de courriel agr.gc.ca) est requis."
    And I should see "Personne (niveau directeur ou supérieur) qui a approuvé l’article (adresse de courriel agr.gc.ca) est requis."
    And I should see "Le champ Catégorie est requis."
    And I should see "À : est requis."
    And I should see "Titre (Français) est requis."
    And I should see "Titre (Anglais) est requis."
    And I should see "De : (Français) est requis."
    And I should see "De : (Anglais) est requis."
    And I should see "Le champ Article (Français) est requis."
    And I should see "Le champ Article (Anglais) est requis."
    And I should see "Le champ Numéro de demande de traduction est requis."
    And I should see "Date est requis."
    And I should see "Nom est requis."
    And I should see "Le champ Adresse de courriel (agr.gc.ca) est requis."
    And I should see "Le champ Numéro de téléphone est requis."
    And I should not see "Message d'erreur"

    # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-en-0-value" field

  # Add input or make selections for each mandatory field
    And I fill in the following:
      | edit-field-newsreviewedby-0-value | agsbehat-news-tester@yopmail.com |
      | edit-field-newsapprovedby-0-value | agsbehat-news-tester@yopmail.com |
    When I select option "Avis généraux" from "edit-field-newstype"
    When I select option "Tous les employés" from "edit-field-to"
    Then I fill in the following:
      | edit-title-0-value | Test Headline FR (FR form) |
      | edit-title-etuf-en-0-value | Test Headline EN (FR form) |
      | edit-field-from-0-value | agsbehat-news-tester@yopmail.com |
      | edit-field-from-etuf-en-0-value | agsbehat-news-tester@yopmail.com |
    And I fill in wysiwyg on field "edit-body-0-value" with "Test EN article text. (FR form)"
    And I fill in wysiwyg on field "edit-body-etuf-en-0-value" with "Test FR article text. (FR form)"
    Then I fill in the following:
      | edit-field-newstranslationnum-0-value | 123456 |
      | edit-field-posted-0-value-date | 06/09/2020 |
      | edit-field-newssubmittername-0-value | Test News Submitter Name (FR form) |
      | edit-field-newssubmitteremail-0-value | agsbehat-news-tester@yopmail.com |
      | edit-field-newssubmitterphone-0-value | Test News Submitter Phone 12345 (FR form)|

  # Verify each validation error is now gone
    When I pause 1 seconds
    Then I should not see "Conseiller/e en communications qui a revu l’article (adresse de courriel agr.gc.ca) est requis."
    And I should not see "Personne (niveau directeur ou supérieur) qui a approuvé l’article (adresse de courriel agr.gc.ca) est requis."
    And I should not see "Catégorie est requis."
    And I should not see "À :  est requis."
    And I should not see "Titre (Français) est requis."
    And I should not see "Titre (Anglais) est requis."
    And I should not see "De (Français) est requis."
    And I should not see "De (Anglais) est requis."
#    And I should not see "Article (Français) est requis."
#    And I should not see "Article (Anglais) est requis."
    And I should not see "Numéro de demande de traduction est requis."
    And I should not see "Date est requis."
    And I should not see "Nom est requis."
    And I should not see "Adresse de courriel (agr.gc.ca) est requis."
    And I should not see "Numéro de téléphone est requis."
    And I should not see "Message d'erreur"

  # TODO - Only mandatory fields are validated, non-mandatory fields and not tested currently
    # Add Media - Fichiers connexes
    # Notes - Commentaires

  # Verify functionality of the button ... Preview
    When I click the "#edit-preview" button
    And I pause 10 seconds
    Then I should see "Revenir à la modification du contenu"
    And I should see "Test Headline FR (FR form)"

  # Verify functionality of the link ... Return to form in progress
    When I click the "#edit-backlink" button
    And I pause 10 seconds
    Then I should not see "Back to content editing"

  # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 10 seconds

  # Successful Submit should display a Confirmation message
    And I should see "a été créé"
  # Successful Submit should auto-forward to the Home Page
    Then I should see "Trouver un collègue"

 # Clean up post-test - delete the content created by this test
 #  Given I am on "/user/login"
 #  Then I should see "Log in"
 #  When I fill in "edit-name" with "agrisourcebehat_adm"
 #  And I fill in "edit-pass" with "PasswordTestLongMotDePasse1"
 #  And I click the "#edit-submit" button
 #  Then I should see "Moderation Dashboard" in the "h1" element
 #  Given I am on "/admin/content/behat"
 #  And I pause 2 seconds
 #  And I fill in "edit-title" with "Test Headline EN (EN form)"
 #  And I click the "#edit-submit-content" button
 #  And I pause 5 seconds
 #  Then I should see "Test Headline EN (EN form) delete link"
 #  When I click "Test Headline EN (EN form) delete link"
 #  And I pause 5 seconds
 #  Then I click the "#edit-submit" button
 #  Then I should see "Test Headline EN (EN form) has been deleted"
 #  And I fill in "edit-title" with "Test Headline EN (FR form)"
 #  And I click the "#edit-submit-content" button
 #  And I pause 5 seconds
 #  Then I should see "Test Headline EN (FR form) delete link"
 #  When I click "Test Headline EN (FR form) delete link"
 #  And I pause 5 seconds
 #  Then I click the "#edit-submit" button
 #  Then I should see "Test Headline EN (FR form) has been deleted"
