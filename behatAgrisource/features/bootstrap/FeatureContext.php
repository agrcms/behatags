<?php

// /*use Drupal\DrupalExtension\Context\RawDrupalContext;*/
// comment following two lines out when using DrupalExtension
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
// comment following two lines out when using DrupalExtension
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;

/**
 * Defines application features from the specific context.
 */
//class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {
class FeatureContext extends RawMinkContext implements Context, SnippetAcceptingContext {
  /**
   * Parameters array.
   *
   * @var array
   */
  protected $parameters;

  /**
   * Environment array.
   * @var array
   */
  protected $environment;

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   * @param array $base_url
   *   Parameters from the behat.yml
   */
  public function __construct($parameters) {
    $this->parameters = $parameters;
  }

  /**
   * Run before every scenario.
   *
   * @BeforeScenario
   */
  public function beforeScenario(BeforeScenarioScope $scope) {
    // Load and save the environment for each scenario.
    $this->environment = $scope->getEnvironment();

    $driver = $this->getSession()->getDriver();
    if ($driver instanceof Selenium2Driver) {
      // Continue if this is a Selenium driver, since this is handled in
      // locatePath().
    }
    else {
      //throw new Exception('not an instance of Selenium2Driver');
      // Setup basic auth.
      //$this->getSession()->setBasicAuth('pwp', 'eye-T$B33!');
    }
  }

  /**
   * Sets the base URL for all environments.
   *
   * @param string $url
   *   The url to set.
   *
   * @see: https://github.com/Behat/MinkExtension/issues/155#issuecomment-77041296
   */
  private function setBaseUrl($url) {
    foreach ($this->environment->getContexts() as $context) {
      if ($context instanceof \Behat\MinkExtension\Context\RawMinkContext) {
        $context->setMinkParameter('base_url', $url);
        $this->setMinkParameter('base_url', $url);
      }
      else {
        throw new PendingException('$context is not an instanceof RawMinkContext.');
      }
    }
  }

  private function getBaseUrl() {
    foreach ($this->environment->getContexts() as $context) {
      if ($context instanceof \Behat\MinkExtension\Context\RawMinkContext) {
        return rtrim($context->getMinkParameter('base_url'));
      }
      else {
        throw new PendingException('$context is not an instanceof RawMinkContext.');
      }
    }
  }

 /**
   * Override MinkContext::locatePath() to work around Selenium not supporting
   * basic auth.
   */
  public function locatePath($path) {
    $driver = $this->getSession()->getDriver();
    if ($driver instanceof Selenium2Driver) {
      // Add the basic auth parameters to the base url. This only works for
      // Firefox.
      $startUrl = rtrim($this->getMinkParameter('base_url'), '/') . '/';
      return true; //// THIS DISABLES BASIC AUTH CODE BELOW, IF NOT USING BASIC AUTH, COMPATIBILITY ISSUE WITH Selenium2Driver. Will review asap
      $startUrl = str_replace('://', '://' . 'pwp' . ':' . 'eye-T$B33!'
 . '@', $startUrl);
      return 0 !== strpos($path, 'http') ? $startUrl . ltrim($path, '/') : $path;
    }
    else {
      return parent::locatePath($path);
    }
  }

  /**
   * @When I run the test in English
   * @When English
   * @When anglais
   */
  public function iRunTheTestInEnglish() {
    //Function to change the base URL back to English, needed for all frontend tests.
    $base_url = $this->parameters['base_url'];
    $this->setBaseUrl($base_url);
  }

  /**
   * @When I run the test in French
   * @When French
   * @When francais
   */
  public function iRunTheTestInFrench() {
    $base_url = $this->parameters['base_url'];
    $base_url = rtrim($base_url);
    if (stripos($base_url, '/en') >= 1 ) {
      $base_url = str_replace('/en','/fr', $base_url);
      $this->setBaseUrl($base_url);
    }
    else {
      //throw new PendingException();
      throw new Exception('unable to switch language from french, Talk to Joseph Olstad.');
    }
  }

 /**
   * @And I wait :arg1 seconds for an ajax call
  */
  public function iWaitSecondsForAnAjaxCall($arg1)
  {
    $wait = $arg1;
    $time = time();
    $stopTime = $time + $wait;
    while (time() < $stopTime)
    {
        try {
            if (time() >= $stopTime) {
                return;
            }
        } catch (\Exception $e) {
            // do nothing
        }

        usleep(250000);
    }

    //$this->getSession()->wait($time, '(0 === Ajax.activeRequestCount)');
    // asserts below
    //throw new PendingException();
  }

  /**
   * @Given je pause :arg1 seconds
   */
  public function jePauseSeconds($arg1)
  {
    $this->iPauseSeconds($arg1);
  }

  /**
   * @Then I pause :arg1 seconds
   */
  public function iPauseSeconds($arg1)
  {
    $this->iWaitSecondsForAnAjaxCall($arg1);
  }

  /**
   * @Given I wait :arg1 seconds for an ajax call
   */
  public function iWaitSecondsForAnAjaxCall2($arg1)
  {
    $this->iWaitSecondsForAnAjaxCall($arg1);
  }

 /**
  * Click on the element with the provided CSS Selector
  *
  * @When /^I click on the element with css selector "([^"]*)"$/
  *
  */
  public function iClickOnTheElementWithCSSSelector($cssSelector)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    if (stripos($base_url,'/tma') > 0) {
      $this->iPauseSeconds(1);
      // Workaround for popup that sometimes occurs on tma site when path is /my-tenders
      $driver = $this->getSession()->getDriver();
      $driver->executeScript("javascript:try {document.getElementById('popup-announcement-close').click();} catch (err) {console.log('close the announcement popup.');}");
      $this->iPauseSeconds(1);
    }
    $session = $this->getSession();
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
    );
    if (null === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
    }
    $element->click();

  }

 /**
  * Xpath example = "//tagtype[@id='id']"
  * Xpath example = "//div[@id='id']"
  * find an element of tagtype by CSS class with XPATH = "//tagtype[contains(concat(' ', @class, ' '), ' example-class ')]"
  * find an element of tagtype by CSS class with XPATH = "//div[contains(concat(' ', @class, ' '), ' example-class ')]"
  * @When I wait for :arg1 text to appear in :arg2 element with class :arg3.
  */
  public function iWaitForTextToAppearInElementWithClass($arg1, $arg2, $arg3)
  {
    $cssSelector = $arg2 . '.' . $arg3;
    $driver = $this->getSession()->getDriver();
//exception 'Behat\Mink\Exception\UnsupportedDriverActionException' with message 'JS is not supported by Behat\Mink\Driver\GoutteDriver' in vendor/behat/mink/src/Driver/CoreDriver.php:449
    if (!$driver instanceof Behat\Mink\Driver\GoutteDriver) {
      $driver->wait(10, 1000)->until(
        function () use ($session, $arg1, $cssSelector) {
          $elements = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
          );
          if (empty($elements)) {
            throw new \InvalidArgumentException(sprintf('Could not evaluate element "%s" with class "%s"', $xpath));
          }
          $textFound = 0;
          foreach($elements as $element) {
            if ($arg1 == $element->getValue()) {
              $textFound++;
            }
          }
          return ($textFound > 1 && count($elements) > 0);
        },
        'Error locating' . $arg1
      );
    }
    else {
      throw new Exception('goutte driver cannot do javascript');
    }
  }


  /**
   * @Then I should see the link :arg1
   */
  public function iShouldSeeTheLink($arg1)
  {
    $findName = FALSE;
    try {
      $findName = $this->getSession()->getPage()->findLink($arg1);
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $findName = $this->getSession()->getPage()->findLink($arg1);
      }
      catch (Exception $e) {
        try {
          $this->iPauseSeconds(3);
          $findName = $this->getSession()->getPage()->findLink($arg1);
        }
        catch (Exception $e) {
          $this->iPauseSeconds(5);
          $findName = $this->getSession()->getPage()->findLink($arg1);
        }
      }
    }
    if (!$findName) {
      throw new Exception($arg1 . " could not be found after 9 seconds");
    }
  }

  /**
   * Helper function for I click.
   */
  private function iClickHelper($maxTryNum, $arg1) {
    $arg1=$this->fixStepArgument($arg1);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $findName = $this->getSession()->getPage()->findLink($arg1);
        $findName->click();
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, unable to click \"$arg1\" element was not found on url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, unable to click \"$arg1\" element was not found on url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else {
          $pauseFlag=0;
          //Find out what type of exception this is?. maybe later
          if ($count >= ($maxTryNum -1)) {
            $pauseFlag=1;
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, arg1 matching \"$arg1\" was not found on page. Using url " . $current_url;
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
        // Workaround for popup that sometimes occurs on tma site when path is /my-tenders
        $base_url = $this->getBaseUrl();
        $base_url = rtrim($base_url);
        if (stripos($base_url,'/tma') > 0) {
          $driver = $this->getSession()->getDriver();
          $driver->executeScript("javascript:try {document.getElementById('popup-announcement-close').click();} catch (err) {console.log('close the announcement popup.');}");
        }
      }
      $count++;
    }
  }

  /**
   * @Then I click :arg1
   */
  public function iClick($arg1)
  {
    $this->iClickHelper(14, $arg1);
  }

  /**
   * Helper function for I click xpath :arg1
   */
  private function iClickXpathHelper($maxTryNum, $arg1) {
    $arg1=$this->fixStepArgument($arg1); //was not doing this previously? not sure if needed or not.
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
	$session = $this->getSession();
        $findElement = $session->getPage()->find('xpath', $arg1);
        $findElement->click();
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, unable to click the \"$arg1\" button, it was not found on url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum-1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "FatalThrowableError after $count seconds, arg1 matching \"$arg1\" was not found on page. Using url " . $current_url;
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else {
          $pauseFlag=0;
          //Find out what type of exception this is?.
          if ($count >= ($maxTryNum-1)) {
            $pauseFlag=1;
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, arg1 matching \"$arg1\" was not found on page and threw a weird exception. Using url " . $current_url;
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
        $base_url = $this->getBaseUrl();
        $base_url = rtrim($base_url);
        if (stripos($base_url,'/tma') > 0) {
          // Workaround for popup that sometimes occurs on tma site when path is /my-tenders
          $driver = $this->getSession()->getDriver();
          $driver->executeScript("javascript:try {document.getElementById('popup-announcement-close').click();} catch (err) {console.log('close the announcement popup.');}");
        }
      }
      $count++;
    }
  }

  /**
   * @Then I click xpath :arg1
   */
  public function iClickXpath($arg1)
  {
    $this->iClickXpathHelper(14, $arg1);
  }

  /**
   * Helper function for I click the :arg1 button.
   */
  private function iClickTheButtonHelper($maxTryNum, $arg1) {
    //$arg1=$this->fixStepArgument($arg1); //was not doing this previously? not sure if needed or not.
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $findName = $this->getSession()->getPage()->find("css", $arg1);
        $findName->click();
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, unable to click the \"$arg1\" button, it was not found on url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum-1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "FatalThrowableError after $count seconds, arg1 matching \"$arg1\" was not found on page. Using url " . $current_url;
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else {
          $pauseFlag=0;
          //Find out what type of exception this is?.
          if ($count >= ($maxTryNum-1)) {
            $pauseFlag=1;
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, arg1 matching \"$arg1\" was not found on page. Using url " . $current_url;
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
        $base_url = $this->getBaseUrl();
        $base_url = rtrim($base_url);
        if (stripos($base_url,'/tma') > 0) {
          // Workaround for popup that sometimes occurs on tma site when path is /my-tenders
          $driver = $this->getSession()->getDriver();
          $driver->executeScript("javascript:try {document.getElementById('popup-announcement-close').click();} catch (err) {console.log('close the announcement popup.');}");
        }
      }
      $count++;
    }
  }

  /**
   * @Given I click the :arg1 button
   */
  public function iClickTheButton($arg1)
  {
    $this->iClickTheButtonHelper(14, $arg1);
  }

  public function imgoingtosaythis($what_im_sayin) {
    if ($fp = fopen('z_debug_saythis.txt', 'a')) {
      fwrite($fp, 'debug='.print_r($what_im_sayin, true) . "\n");
      fclose($fp);
    }
  }

  /**
   * Example: Then I should be scrolled to anchor
   * @Then I should be scrolled to anchor
   */
  public function iShouldBeScrolledToAnchor()
  {
      $this->iPauseSeconds(5);
      $driver = $this->getSession()->getDriver();
      $scrollPosition = $driver->evaluateScript("window.pageYOffset || document.documentElement.scrollTop;");
      if ($scrollPosition < 10) {
        throw new Exception("scrollPosition is $scrollPosition , appears not to have scrolled to anchor");
      }
//      else {
//        throw new PendingException("I should be scrolled to anchor: scrollPosition is $scrollPosition , SUCCESS?!??");
//      }
  }

  /**
   * Opens homepage
   * Example: Given I am on "/"
   * Example: When I go to "/"
   * Example: And I go to "/"
   *
   * @Given /^(?:|I )am on (?:|the )homepage$/
   * @When /^(?:|I )go to (?:|the )homepage$/
   */
  public function iAmOnHomepage()
  {
      $this->visitPath('/');
      try {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try {document.getElementById('overridelink').click();} catch (err) {console.log('probably cert warn already accepted');}");
      }
      catch (Exception $e) {
        $this->imgoingtosaythis($e->getMessage());
        // rethrow it
        throw $e;
      }
  }
   /**
   * Opens specified page
   * Example: Given I am on "http://batman.com"
   * Example: And I am on "/articles/isBatmanBruceWayne"
   * Example: When I go to "/articles/isBatmanBruceWayne"
   *
   * @Given /^(?:|I )am on "(?P<page>[^"]+)"$/
   * @When /^(?:|I )go to "(?P<page>[^"]+)"$/
   */
  public function visit($page)
  {
    //IE11 javascript workaround for https unsigned certs.
    try {
      $this->visitPath($page);
      $driver = $this->getSession()->getDriver();
      $driver->executeScript("javascript:try {document.getElementById('overridelink').click();} catch (err) {console.log('probably cert warn already accepted');}");
    }
    catch (Exception $e) {
      $this->imgoingtosaythis($e->getMessage());
      //($e instanceof WebDriver\Exception\Timeout)
      try {
        $this->iPauseSeconds(3);
        $this->visitPath($page);
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try {document.getElementById('overridelink').click();} catch (err) {console.log('probably cert warn already accepted');}");
      } catch (Exception $e) {
        // rethrow it
        $this->imgoingtosaythis($e->getMessage());
        throw $e;
      }
    }
    //Workaround for a popup issue on one particular site , sometimes has a popup.
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    if (stripos($base_url,'/tma') > 0) {
      $this->iPauseSeconds(2);
      // Workaround for popup that sometimes occurs on tma site when path is /my-tenders
      $driver = $this->getSession()->getDriver();
      $driver->executeScript("javascript:try {document.getElementById('popup-announcement-close').click();} catch (err) {console.log('close the announcement popup.');}");
    }
  }

  /**
   * @When /^I hover over the element "([^"]*)"$/
   */
  public function iHoverOverTheElement($locator)
  {
    $session = $this->getSession(); // get the mink session
    $element = $session->getPage()->find('css', $locator); // runs the actual query and returns the element

    // errors must not pass silently
    if (null === $element) {
      throw new \InvalidArgumentException(sprintf('Could not evaluate CSS selector: "%s"', $locator));
    }

    // ok, let's hover it
    $element->mouseOver();
  }
  /**
   * @Given I navigate to :arg1
   */
  public function iNavigateTo($arg1)
  {
    $this->visit($arg1);
  }

   /**
   * Reloads current page
   * Example: When I reload the page
   * Example: And I reload the page
   *
   * @When /^(?:|I )reload the page$/
   */
  public function reload()
  {
      $this->getSession()->reload();
  }
   /**
   * Moves backward one page in history
   * Example: When I move backward one page
   *
   * @When /^(?:|I )move backward one page$/
   * @When /^(?:|I )go back$/
   */
  public function back()
  {
      $this->getSession()->back();
  }
   /**
   * Moves forward one page in history
   * Example: And I move forward one page
   *
   * @When /^(?:|I )move forward one page$/
   * @When /^(?:|I )go forward$/
   */
  public function forward()
  {
      $this->getSession()->forward();
  }
   /**
   * Presses button with specified id|name|title|alt|value
   * Example: When I press "Log In"
   * Example: And I press "Log In"
   *
   * @When /^(?:|I )press "(?P<button>(?:[^"]|\\")*)"$/
   */
  public function pressButton($button)
  {
    $button = $this->fixStepArgument($button);
    $this->getSession()->getPage()->pressButton($button);
  }
   /**
   * Clicks link with specified id|title|alt|text
   * Example: When I follow "Log In"
   * Example: And I follow "Log In"
   *
   * @When /^(?:|I )follow "(?P<link>(?:[^"]|\\")*)"$/
   */
  public function clickLink($link)
  {
      $link = $this->fixStepArgument($link);
      $this->getSession()->getPage()->clickLink($link);
  }
   /**
   * Fills in form field with specified id|name|label|value
   * Example: When I fill in "username" with: "bwayne"
   * Example: And I fill in "bwayne" for "username"
   *
   * @When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
   * @When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with:$/
   * @When /^(?:|I )fill in "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
   */
  public function fillField($field, $value)
  {
    if (strpos($field, '#') === 0) {
      $field = substr($field, 1);
    }
    $field = $this->fixStepArgument($field);
    $value = $this->fixStepArgument($value);
    try {
      $this->getSession()->getPage()->fillField($field, $value);
    } catch (Exception $e) {
      //if ($e instanceof WebDriver\Exception\StaleElementReference) {
      try {
        $this->iPauseSeconds(1);
        $this->getSession()->getPage()->fillField($field, $value);
      } catch (Exception $e) {
        $this->iPauseSeconds(2);
        $this->getSession()->getPage()->fillField($field, $value);
      }
    }
  }
   /**
   * Fills in form fields with provided table
   * Example: When I fill in the following"
   *              | username | bruceWayne |
   *              | password | iLoveBats123 |
   * Example: And I fill in the following"
   *              | username | bruceWayne |
   *              | password | iLoveBats123 |
   *
   * @When /^(?:|I )fill in the following:$/
   */
    public function fillFields(TableNode $fields)
    {
      try {
        foreach ($fields->getRowsHash() as $field => $value) {
          $this->fillField($field, $value);
        }
      } catch (Exception $e) {
        try {
          $this->iPauseSeconds(1);
          foreach ($fields->getRowsHash() as $field => $value) {
            $this->fillField($field, $value);
          }
        } catch (Exception $e) {
          try {
            $this->iPauseSeconds(1);
            foreach ($fields->getRowsHash() as $field => $value) {
              $this->fillField($field, $value);
            }
          } catch (Exception $e) {
            throw $e;
          }
        }
      }
    }

  /**
   * Helper function for select "value or option text" from "form element"
   * Selects option in select field with specified id|name|label|value
   * Example: When I select "Bats" from "user_fears"
   * Example: And I option "Bats" from "user_fears"
   */
  private function selectOptionHelper($maxTryNum, $value, $form_element) {
    $maxTryNum=intval($maxTryNum);
    $value = $this->fixStepArgument($value);
    $form_element = $this->fixStepArgument($form_element);
    $findElement = FALSE;
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->getSession()->getPage()->selectFieldOption($form_element, $value);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $this->iPauseSeconds(1);
          $pauseFlag=1;
        } else if ($e instanceof Behat\Mink\Exception\ExpectationException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, cannot select $value on $form_element, page url is: " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, the \"$form_element\" element was not found on page. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else {
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, cannot select $value on $form_element, page url is: " . $current_url;
	  $pauseFlag=1; // Enough time spent.
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }
  /**
   * Selects option in select field with specified id|name|label|value
   * Example: When I select "Bats" from "user_fears"
   * Example: And I option "Bats" from "user_fears"
   * Example: And I select option "Bats" from "user_fears"
   *
   * @When /^(?:|I )select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
   * @When /^(?:|I )select option "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
   */
  public function selectOption($select, $option)
  {
    if (strpos($select, '#') === 0) {
      $select = substr($select, 1);
    }
    $this->selectOptionHelper(14, $option, $select);
    //$this->getSession()->getPage()->selectFieldOption($select, $option);
  }
   /**
   * Selects additional option in select field with specified id|name|label|value
   * Example: When I additionally select "Deceased" from "parents_alive_status"
   * Example: And I additionally select "Deceased" from "parents_alive_status"
   *
   * @When /^(?:|I )additionally select "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)"$/
   */
  public function additionallySelectOption($select, $option)
  {
    $select = $this->fixStepArgument($select);
    $option = $this->fixStepArgument($option);
    $this->getSession()->getPage()->selectFieldOption($select, $option, true);
  }
  /**
   * Helper function for I check :option
   * Example: Then I check ":option"
   */
  private function checkOptionHelper($maxTryNum, $option) {
    $option=$this->fixStepArgument($option);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->getSession()->getPage()->checkField($option);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $this->iPauseSeconds(1);
          $pauseFlag=1;
        } else if ($e instanceof Behat\Mink\Exception\ExpectationException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, input checkbox with check \"$option\" not found on: " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof WebDriver\Exception\UnknownError) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, and scrollBy 250 px " . $count . " times, input checkbox with check \"$option\" not found on: " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
        else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, input checkbox with check \"$option\" not found on: " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }
 

 /**
   * Checks checkbox with specified id|name|label|value
   * Example: When I force check "Pearl Necklace"
   * Example: And I force check "Pearl Necklace"
   *
   * @When /^(?:|I )force check "(?P<option>(?:[^"]|\\")*)"$/
   */
  public function forceCheckOption($option)
  {
    // Workaround for popup that sometimes occurs on tma site when path is /my-tenders
    $driver = $this->getSession()->getDriver();
    // Begin Drupal 8/9 workaround.
    $driver->executeScript("javascript:try { jQuery('#toolbar-administration').hide(); } catch (err) {console.log('workaround for checkbox is not clickable at point (57, 9). Other element would receive the click.');}");
    $this->checkOptionHelper(14,$option);
    $driver->executeScript("javascript:try { jQuery('#toolbar-administration').show(); } catch (err) {console.log('workaround for checkbox is not clickable at point (57, 9). Other element would receive the click.');}");
  }


 /**
   * Checks checkbox with specified id|name|label|value
   * Example: When I check "Pearl Necklace"
   * Example: And I check "Pearl Necklace"
   *
   * @When /^(?:|I )check "(?P<option>(?:[^"]|\\")*)"$/
   */
  public function checkOption($option)
  {
    $this->checkOptionHelper(14,$option);
  }

  /**
   * Unchecks checkbox with specified id|name|label|value
   * Example: When I uncheck "Broadway Plays"
   * Example: And I uncheck "Broadway Plays"
   *
   * @When /^(?:|I )uncheck "(?P<option>(?:[^"]|\\")*)"$/
   */
  public function uncheckOption($option)
  {
    $option = $this->fixStepArgument($option);
    $this->getSession()->getPage()->uncheckField($option);
  }

  /**
   * Attaches file to field with specified id|name|label|value
   * Example: When I attach "bwayne_profile.png" to "profileImageUpload"
   * Example: And I attach "bwayne_profile.png" to "profileImageUpload"
   *
   * @When /^(?:|I )attach the file "(?P<path>[^"]*)" to "(?P<field>(?:[^"]|\\")*)"$/
   */
  public function attachFileToField($field, $path)
  {
    $field = $this->fixStepArgument($field);

    if ($this->getMinkParameter('files_path')) {
      $fullPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$path;
      if (is_file($fullPath)) {
        $path = $fullPath;
      }
    }

    $this->getSession()->getPage()->attachFileToField($field, $path);
  }

  /**
   * Helper function for I should be on :page
   * Example: Then I should be on "/"
   */
  private function assertPageAddressHelper($maxTryNum, $page) {
    $page=$this->fixStepArgument($page);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->addressEquals($this->locatePath($page));
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        if ($e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $this->iPauseSeconds(1);
          $pauseFlag=1;
        } else if ($e instanceof Behat\Mink\Exception\ExpectationException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, not on \"$page\" as expected, instead am on: " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, not on \"$page\" as expected, instead am on: " . $current_url;
	  $pauseFlag=1; // Enough time spent.
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that current page PATH is equal to specified
   * Example: Then I should be on "/"
   * Example: And I should be on "/bats"
   * Example: And I should be on "http://google.com"
   *
   * @Then /^(?:|I )should be on "(?P<page>[^"]+)"$/
   */
  public function assertPageAddress($page)
  {
    $this->assertPageAddressHelper(15,$page);
  }
  /**
   * @Given je visite :arg1
   * @Soit je visite :arg1
   */
  public function jeVisite($arg1)
  {
    $this->visit($arg1);
  }

  /**
   * @Then je devrait être sur :arg1
   * @Alors je devrait être sur :arg1
   */
  public function jeDevraitEtreSur($arg1)
  {
    $this->assertPageAddress($arg1);
  }
  /**
   * Checks, that current page is the homepage
   * Example: Then I should be on the homepage
   * Example: And I should be on the homepage
   *
   * @Then /^(?:|I )should be on (?:|the )homepage$/
   */
  public function assertHomepage()
  {
    try {
      $this->assertSession()->addressEquals($this->locatePath('/'));
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->addressEquals($this->locatePath('/'));
      }
      catch (Exception $e) {
        try {
          $this->iPauseSeconds(2);
          $this->assertSession()->addressEquals($this->locatePath('/'));
        }
        catch (Exception $e) {
          $this->iPauseSeconds(4);
          $this->assertSession()->addressEquals($this->locatePath('/'));
        }
      }
    }
  }

  /**
   * Checks, that current page PATH matches regular expression
   * Example: Then the url should match "superman is dead"
   * Example: Then the uri should match "log in"
   * Example: And the url should match "log in"
   *
   * @Then /^the (?i)url(?-i) should match (?P<pattern>"(?:[^"]|\\")*")$/
   */
  public function assertUrlRegExp($pattern)
  {
    try {
      $this->assertSession()->addressMatches($this->fixStepArgument($pattern));
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->addressMatches($this->fixStepArgument($pattern));
      }
      catch (Exception $e) {
        try {
          $this->iPauseSeconds(2);
          $this->assertSession()->addressMatches($this->fixStepArgument($pattern));
        }
        catch (Exception $e) {
          $this->iPauseSeconds(4);
          $this->assertSession()->addressMatches($this->fixStepArgument($pattern));
        }
      }
    }
  }

  /**
   * Checks, that current page response status is equal to specified
   * Example: Then the response status code should be 200
   * Example: And the response status code should be 400
   *
   * @Then /^the response status code should be (?P<code>\d+)$/
   */
  public function assertResponseStatus($code)
  {
    $this->assertSession()->statusCodeEquals($code);
  }

  /**
   * Checks, that current page response status is not equal to specified
   * Example: Then the response status code should not be 501
   * Example: And the response status code should not be 404
   *
   * @Then /^the response status code should not be (?P<code>\d+)$/
   */
  public function assertResponseStatusIsNot($code)
  {
    $this->assertSession()->statusCodeNotEquals($code);
  }

  /**
   * Unified login for simple catalogue, works on dev and testing AND in beta and should work in content as well.
   * @When /^(?:|I )log into simple catalogue with username "(?P<username>(?:[^"]|\\")*)" password "(?P<password>(?:[^"]|\\")*)"$/
   *
   */
  public function iLogIntoSimpleCatalogueWithUsernamePassword($username, $password) {
    $this->iLogIntoSosaaWithUsernamePassword($username, $password);
  }

  /**
   * Unified login for dwp (donor web portal).
   * @When /^(?:|I )log into dwp with username "(?P<username>(?:[^"]|\\")*)" password "(?P<password>(?:[^"]|\\")*)"$/
   *
   */
  public function iLogIntoDwpWithUsernamePassword($username, $password) {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $this->iNavigateTo('/user/login'); // works when the base url is for a site (Drupal 6, Drupal 7 and Drupal 8 with a standard drupal /user/login form.
    $test_url = $this->getSession()->getCurrentUrl();
    $logged_in = TRUE;
    $english = FALSE;
    $lowThreshold = 2;
    if (stripos($test_url, '/login') >= 1) {
      $logged_in = FALSE;
    } else {
      $this->iNavigateTo('/user/logout'); // Sure logged out.
      $logged_in = FALSE;
      $this->iPauseSeconds(1); // Give a bit of time for logout.
      $this->iNavigateTo('/user/login');
      $this->iPauseSeconds(1); // Give a bit of time.
    }
    try {
      //Membre depuis
      //Member since
      //Demander un nouveau mot de passe
      //Request new password
      $this->assertPageContainsText('Forgot your password', $lowThreshold);
      $english = TRUE;
    } catch (Exception $e) {
      // Maybe drupal site is in french, OR see catch.
      $this->assertPageContainsText("Nom d'utilisateur", $lowThreshold);
      $english = FALSE;
    }
    //Standard Drupal login.
    $this->fillField('edit-name', $username);
    $this->fillField('edit-pass', $password);
    $this->iClickTheButton("#edit-submit");
  }

  /**
   * One step login for a quick drupal login.
   * @When /^(?:|I )log in quickly with username "(?P<username>(?:[^"]|\\")*)" password "(?P<password>(?:[^"]|\\")*)"$/
   *
   */
  public function iLogInQuicklyWithUsernamePassword($username, $password) {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $this->iNavigateTo('/user/login'); // works when the base url is for a site (Drupal 6, Drupal 7 and Drupal 8 with a standard drupal /user/login form.
    $test_url = $this->getSession()->getCurrentUrl();
    $logged_in = TRUE;
    $english = FALSE;
    $lowThreshold = 2;
    if (stripos($test_url, '/login') >= 1) {
      $logged_in = FALSE;
    } else {
      $this->iNavigateTo('/user/logout'); // Sure logged out.
      $logged_in = FALSE;
      $this->iPauseSeconds(1); // Give a bit of time for logout.
      $this->iNavigateTo('/user/login');
      $this->iPauseSeconds(1); // Give a bit of time.
    }
    try {
      //Membre depuis
      //Member since
      //Demander un nouveau mot de passe
      //Request new password
      //Reset your password
      $this->assertPageContainsText('Reset your password', $lowThreshold);
      $english = TRUE;
    } catch (Exception $e) {
      // Maybe drupal site is in french, OR see catch.
      $this->assertPageContainsText('Réinitialiser votre mot de passe', $lowThreshold);
      $english = FALSE;
    }
    //Standard Drupal login.
    $this->fillField('edit-name', $username);
    $this->fillField('edit-pass', $password);
    $this->iClickTheButton("#edit-submit");
  }


  /**
   * One step login for a standard drupal login, simple drupal login.
   * @When /^(?:|I )log into drupal with username "(?P<username>(?:[^"]|\\")*)" password "(?P<password>(?:[^"]|\\")*)"$/
   *
   */
  public function iLogIntoDrupalWithUsernamePassword($username, $password) {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $this->iNavigateTo('/user/login'); // works when the base url is for a site (Drupal 6, Drupal 7 and Drupal 8 with a standard drupal /user/login form.
    $test_url = $this->getSession()->getCurrentUrl();
    $logged_in = TRUE;
    $english = FALSE;
    $lowThreshold = 2;
    if (stripos($test_url, '/login') >= 1) {
      $logged_in = FALSE;
    } else {
      $this->iNavigateTo('/user/logout'); // Sure logged out.
      $logged_in = FALSE;
      $this->iPauseSeconds(1); // Give a bit of time for logout.
      $this->iNavigateTo('/user/login');
      $this->iPauseSeconds(1); // Give a bit of time.
    }
    try {
      //Membre depuis
      //Member since
      //Demander un nouveau mot de passe
      //Request new password
      $this->assertPageContainsText('Request new password', $lowThreshold);
      $english = TRUE;
    } catch (Exception $e) {
      // Maybe drupal site is in french, OR see catch.
      $this->assertPageContainsText('Demander un nouveau mot de passe', $lowThreshold);
      $english = FALSE;
    }
    //Standard Drupal login.
    $this->fillField('edit-name', $username);
    $this->fillField('edit-pass', $password);
    $this->iClickTheButton("#edit-submit");
  }

  /**
   * One step login for pdata, simpler than the sosaa login.
   * @When /^(?:|I )log into pdata with username "(?P<username>(?:[^"]|\\")*)" password "(?P<password>(?:[^"]|\\")*)"$/
   *
   */
  public function iLogIntoPdataWithUsernamePassword($username, $password) {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $logged_in = FALSE;
    $password_string = 'Password';
    $english = TRUE;
    $pors_gcaccounts_login = FALSE;
    $this->iNavigateTo('/user'); // /procurement-data portion is in the string.
    $test_url = $this->getSession()->getCurrentUrl();
    if (stripos($base_url, 'buyandsell') >= 1 || stripos($base_url, 'achatsetventes') >= 1
           || stripos($base_url, '.en.') >= 1 || stripos($base_url, '.fr.') >= 1 ) {
      if (stripos($test_url, 'idp/cdcservlet') >= 1) {
        $logged_in = FALSE;
        $pors_gcaccounts_login = TRUE;
      }
      if (stripos($base_url, 'buyandsell') >= 1 || stripos($base_url, '.en.') >= 1) {
        $english = TRUE;
        $password_string = 'Password';
      }
      if (stripos($base_url, 'achats') || stripos($base_url, '.fr.') >= 1) {
        $english = FALSE;
        $password_string = 'Mot de passe';
      }
      $this->pageContainsTextHelper(13,$this->fixStepArgument('Canada'));
      try {
        $this->pageContainsTextHelper(3, $password_string);
      } catch (Exception $e) {
        $logged_in = TRUE;
      }
      //$this->assertPageContainsText('Mot de passe');
    }
    if ($logged_in) {
      $this->iNavigateTo('/user/logout'); // /procurement-data portion is in the string.
      $this->iPauseSeconds(1);
      $this->iNavigateTo('/user'); // /procurement-data portion is in the string.
      $test_url = $this->getSession()->getCurrentUrl();
      if (stripos($test_url, 'idp/cdcservlet') >= 1) {
        $logged_in = FALSE;
        $pors_gcaccounts_login = TRUE;
      }
    }
    if ($pors_gcaccounts_login) {
      $this->fillField('IDToken1', $username);
      $this->fillField('IDToken2', $password);
      $this->iHoverOverTheElement("div.logBtn");
      $this->iClickTheButton(".Btn1DefHov");
      $this->iPauseSeconds(1);
      if ($english) {
        $this->assertPageContainsText('Home');
      } else {
        $this->assertPageContainsText('Accueil');
      }
    } else {
      $this->fillField('edit-name', $username);
      $this->fillField('edit-pass', $password);
      $this->iClickTheButton("#edit-submit");
    }
  }

  /**
   * @Then I log out if we see :arg1 in the :arg2 element
   */
  public function iLogOutIfWeSeeInTheElement($arg1, $arg2)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $text = $this->fixStepArgument($arg1);
    $element = $arg2;
      $js = <<<JS
(function myFunction() {
  //function content
  var textInElement = jQuery("$element").text();
  if (typeof(textInElement) == "undefined") {
    return false;
  } else {
    if (textInElement == "$text") {
      return true;
    } else {
      return false;
    }
  }
  })();
JS;

    try {
      $this->pageTextNotContainsHelper(3,$this->fixStepArgument($text));
    }
    catch (Exception $e) {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $textInElement = $driver->evaluateScript($js);
      if ($textInElement) {
        $this->iClickOnTheElementWithCSSSelector($element);
        $this->iPauseSeconds(1);
        $this->iNavigateTo('/');
        $this->iPauseSeconds(1);
      }
    }
  }


  /**
   * @Then I ask behat to disable the confirm publish dialog
   */
  public function iAskBehatToDisableTheConfirmPublishDialog()
  {
    $this->getSession()->executeScript("jQuery('*').unbind('click.moderated_content_bulk_publish');");
  }


  /**
   * @Then I ask behat to disable the confirm revive alert
   */
  public function iAskBehatToDisableTheConfirmReviveAlert() {
    $this->getSession()->executeScript("jQuery('*').unbind('click.agriAdmin');");
  }


  /**
   * @Then I ask behat to disable html5 form validation for the :locator field
   */
  public function iAskBehatToDisableHtmlFormValidationForTheField($locator)
  {
    $el = $this->getSession()->getPage()->findField($locator);
    if (empty($el)) {
      throw new ExpectationException('Could not find the field with locator: ' . $locator, $this->getSession());
    }

    $fieldId = $el->getAttribute('id');

    if (empty($fieldId)) {
      throw new Exception('Could not find an id for field with locator: ' . $locator);
    }

    $this->getSession()->executeScript("var fTmp = $(\"#$fieldId\"); $(fTmp).attr('aria-required', 'false'); $(fTmp).removeClass('required error'); $(fTmp).attr('aria-invalid', 'false');$(fTmp).parent().find('label').hide();");
  }


  /**
   * @Then I fill in wysiwyg on field :locator with :value
   */
  public function iFillInWysiwygOnFieldWith($locator, $value) {
    $el = $this->getSession()->getPage()->findField($locator);

    if (empty($el)) {
      throw new ExpectationException('Could not find WYSIWYG with locator: ' . $locator, $this->getSession());
    }

    $fieldId = $el->getAttribute('id');

    if (empty($fieldId)) {
      throw new Exception('Could not find an id for field with locator: ' . $locator);
    }
    $this->getSession()->executeScript("CKEDITOR.instances[\"$fieldId\"].setData(\"$value\");");
  }


  /**
   * @Then I click the :arg1 element with :arg2 attribute containing :arg3
   */
  public function iClickTheElementWithAttributeContaining($arg1, $arg2, $arg3)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $locator = $arg1;
    $attribute = $arg2;
    $text = $arg3;
      $js = <<<JS
(function myFunction() {
  //function content
  var textSearch = "$text";
  var attribute = "$attribute";
  var locator = "$locator";
  var testElement = jQuery(locator + "["+attribute+"*='"+textSearch+"']");
  console.log(locator + "["+attribute+"*='"+textSearch+"']");
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    testElement.trigger('mouseenter');
    testElement.trigger('mouseover');
    testElement.trigger('click');
    testElement.trigger('mouseout');
    return true;
  }
  })();
JS;

   try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = $driver->evaluateScript($js);
      if (!$jsSuccess) {
        throw new Exception('js did not work for iClickTheElementWithAttributeContaining() using parameters locator' . $locator . ' attribute:' . $attribute . ' text:' . $text);
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I validate the award not feeding :arg1 using the key in li containing strong text :arg2
   */
  public function iValidateTheAwardNotFeedingUsingTheKeyInLiContainingStrongText($arg1, $arg2)
  {
    $this->iPauseSeconds(1);
    $title_of_deleted = $arg1;
    $text = $arg2;
      $js = <<<JS
(function myFunction() {
  //function content
  var testElement = jQuery('ul li strong:contains(' + "$text" + ')').parent();
  var textWithCodeAndStrong = testElement.text();
  var strongTextOnly = jQuery('ul li strong:contains(' + "$text" + ')').contents().unwrap().text();
  var codeOnly = textWithCodeAndStrong.replace(strongTextOnly, '').trim();

  if (typeof(testElement) == "undefined" && typeof(codeOnly) != "string") {
    return false;
  } else {
    if (codeOnly.length > 4) {
      return codeOnly;
    } else {
      return false;
    }
  }
  })();
JS;

    $this->iPauseSeconds(2);
    $driver = $this->getSession()->getDriver();
    $jsSuccess = FALSE;
    $jsSuccess = $driver->evaluateScript($js);

    if ($jsSuccess == FALSE) {
      $current_url = $this->getSession()->getCurrentUrl();
      $this->pageContainsTextHelper(2,$this->fixStepArgument('iValidateTheAwardFeedUsingTheKeyInLiContainingStrongText function failure FeatureContext.php, could not find ' . $text . ' or the key that was found was not valid are we on the correct page? current_url is= ' . $current_rul ));
    } else {
      $key_returned = $jsSuccess;
      $this->visit($this->getBaseUrl() . "/feed/award?key=" . $key_returned);
      $page_content = $this->getSession()->getDriver()->getContent();
      if (strpos($page_content, $title_of_deleted) < 1) {
        $this->assertPageAddressHelper(2, "/feed/award?key=" . $key_returned);
      } else {
	throw new Exception('/tma/feed/award?key=' . $key_returned . ' DELETED ITEM NAMED ' . $title_of_deleted . ' IS FEEDING, BUT SHOULD NOT BE');
      }
    }
  }

  /**
   * @Then I validate the award feed using the key in li containing strong text :arg
   */
  public function iValidateTheAwardFeedUsingTheKeyInLiContainingStrongText($arg1)
  {
    $this->iPauseSeconds(1);
    $text = $arg1;
      $js = <<<JS
(function myFunction() {
  //function content
  var testElement = jQuery('ul li strong:contains(' + "$text" + ')').parent();
  var textWithCodeAndStrong = testElement.text();
  var strongTextOnly = jQuery('ul li strong:contains(' + "$text" + ')').contents().unwrap().text();
  var codeOnly = textWithCodeAndStrong.replace(strongTextOnly, '').trim();

  if (typeof(testElement) == "undefined" && typeof(codeOnly) != "string") {
    return false;
  } else {
    if (codeOnly.length > 4) {
      return codeOnly;
    } else {
      return false;
    }
  }
  })();
JS;

    $this->iPauseSeconds(2);
    $driver = $this->getSession()->getDriver();
    $jsSuccess = FALSE;
    $jsSuccess = $driver->evaluateScript($js);

    if ($jsSuccess == FALSE) {
      $current_url = $this->getSession()->getCurrentUrl();
      $this->pageContainsTextHelper(2,$this->fixStepArgument('iValidateTheAwardFeedUsingTheKeyInLiContainingStrongText function failure FeatureContext.php, could not find ' . $text . ' or the key that was found was not valid are we on the correct page? current_url is= ' . $current_rul ));
    } else {
      $key_returned = $jsSuccess;
      $this->visit($this->getBaseUrl() . "/feed/award?key=" . $key_returned);
      $page_content = $this->getSession()->getDriver()->getContent();
      if (strpos($page_content, 'Award notices') >= 1 && strpos($page_content, 'Avis ') >= 1) {
        $this->assertPageAddressHelper(2, "/feed/award?key=" . $key_returned);
      } else {
	throw new Exception('/tma/feed/award?key=' . $key_returned . ' IS NOT FEEDING, BUT IT SHOULD.');
      }
    }
  }

  /**
   * @Then I add name :arg1 for :arg2 element
   */
  public function iAddNameForElement($arg1, $arg2)
  {
    $this->iPauseSeconds(1);
    $locator = $arg2;
    $name = $arg1;
      $js = <<<JS
(function myFunction() {
  //function content
  var nameAttribute = "$name";
  var testElement = jQuery("$locator");
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    testElement.attr('name',"$name");
    return true;
  }
  })();
JS;

    $this->iPauseSeconds(2);
    $driver = $this->getSession()->getDriver();
    $jsSuccess = FALSE;
    $jsSuccess = $driver->evaluateScript($js);

    if (!$jsSuccess) {
      $this->pageContainsTextHelper(2,$this->fixStepArgument('iAddNameForElement function failure FeatureContext.php, could not find ' . $locator));
    }
  }

  /**
   * @Then I click a link in element :arg1 containing title :arg2 status :arg3
   */
  public function iClickALinkInElementContainingTitleStatus($arg1, $arg2, $arg3)
  {
    $this->iPauseSeconds(1);
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $locator = $arg1;
    $text = $arg2;
    $statustext = $arg3;
      $js = <<<JS
(function myFunction() {
  //function content
  var textSearch = "$text";
  var statusText = "$statustext";
  var testElement = jQuery("$locator"+':contains(' + textSearch + ')').find('td:contains(' + statusText + ')').parent().find('a:contains(' + textSearch + ')');
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    return testElement.attr('href');
  }
  })();
JS;

   try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = FALSE;
      $href = $driver->evaluateScript($js);
      if (strlen($href) > 7 && stripos($href, 'ttp') >= 1) {
        $jsSuccess = TRUE;
      }

      if ($jsSuccess) {
        $url1 = 'https://blah';
        $url2 = 'https://blah/blah';
        $relativeLink = str_replace($this->getBaseUrl(), '', $href);
        $this->visit($relativeLink);
        $this->iPauseSeconds(1);
        $this->pageContainsTextHelper(12,$this->fixStepArgument($text));
      } else {
        $this->pageTextNotContainsHelper(4,$this->fixStepArgument($text));
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I book the :arg1 appointment
   */
  public function iBookTheAppointment($arg1)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $locator = 'label.option';
    $text = $arg1;
      $js = <<<JS
(function myFunction() {
  //function content
  var textSearch = "$text";
  var testElement = jQuery("$locator"+':contains(' + textSearch + ')').parent();
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    setTimeout(function(){
      var textSearch = "$text";
      var testElement = jQuery("$locator"+':contains(' + textSearch + ')').parent();
      var form = testElement.closest('.cbs-wss-booking-form-clinic-select-event-time');
      form.find('.form-item-appointmentTime').removeClass('selected');
      var timeLabel = testElement.closest('label');
      timeLabel.trigger('focus');
      timeLabel.trigger('hover');
      timeLabel.trigger('mousedown');
      timeLabel.trigger('click');
      testElement.trigger('focus');
      testElement.trigger('hover');
      testElement.trigger('mousedown');
      testElement.trigger('click');
      var timeInput = testElement.find('input');
      timeInput.trigger('focus');
      timeInput.trigger('hover');
      timeInput.trigger('mousedown');
      timeInput.trigger('click');
      testElement.addClass('selected');
      form.submit();
    }, 50);
    return true;
  }
  })();
JS;

   try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = $driver->evaluateScript($js);
      if ($jsSuccess) {
        $this->pageContainsTextHelper(12,$this->fixStepArgument($text));
      } else {
        $this->pageTextNotContainsHelper(2,$this->fixStepArgument($text));
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I click the parent element of :arg1 element containing :arg2
   */
  public function iClickTheParentElementOfElementContaining($arg1, $arg2)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $locator = $arg1;
    $text = $arg2;
      $js = <<<JS
(function myFunction() {
  //function content
  var textSearch = "$text";
  var testElement = jQuery("$locator"+':contains(' + textSearch + ')').parent();
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    testElement.trigger('mouseenter');
    testElement.trigger('mouseover');
    testElement.trigger('click');
    testElement.trigger('mouseout');
    return true;
  }
  })();
JS;

   try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = $driver->evaluateScript($js);
      if ($jsSuccess) {
        $this->pageContainsTextHelper(2,$this->fixStepArgument($text));
      } else {
        $this->pageTextNotContainsHelper(2,$this->fixStepArgument($text));
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I click parent element of element containing :arg1
   */
  public function iClickParentElementOfElementContaining($arg1)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $text = $arg1;
      $js = <<<JS
(function myFunction() {
  //function content
  var textSearch = "$text";
  var testElement = jQuery(':contains(' + textSearch + ')').parent();
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    testElement.trigger('mouseenter');
    testElement.trigger('mouseover');
    testElement.trigger('click');
    testElement.trigger('mouseout');
    return true;
  }
  })();
JS;

   try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = $driver->evaluateScript($js);
      if ($jsSuccess) {
        $this->pageContainsTextHelper(2,$this->fixStepArgument($text));
      } else {
        $this->pageTextNotContainsHelper(2,$this->fixStepArgument($text));
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I click parent :arg1 element of first element containing :arg2
   */
  public function iClickParentElementOfFirstElementContaining($arg1, $arg2)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $element = $arg1;
    $text = $arg2;
      $js = <<<JS
(function myFunction() {
  //function content
  var elementTest = "$element";
  var textSearch = "$text";
  var testElement = jQuery(':contains(' + textSearch + ')').parent();
  if (testElement.length > 1) {
    testElement = jQuery(':contains(' + textSearch + ')').closest(elementTest).parent();
  }
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    testElement.trigger('mouseenter');
    testElement.trigger('mouseover');
    testElement.trigger('click');
    testElement.trigger('mouseout');
    return true;
  }
  })();
JS;

   try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = $driver->evaluateScript($js);
      if ($jsSuccess) {
        $this->pageContainsTextHelper(2,$this->fixStepArgument($text));
      } else {
        $this->pageTextNotContainsHelper(2,$this->fixStepArgument($text));
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I make :arg1 visible
   */
  public function iMakeVisible($arg1)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $element = $arg1;
      $js = <<<JS
(function myFunction() {
  //function content
  var testElement = jQuery("$element");
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    jQuery("$element").show();
    jQuery("$element").css('visibility', 'visible');
    return true;
  }
  })();
JS;

    try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $jsSuccess = $driver->evaluateScript($js);
      if (!$jsSuccess) {
        $this->assertSession()->pageTextContains($this->fixStepArgument($element));
      }
    }
    catch (Exception $e) {
      throw $e;
    }
  }

  /**
   * @Then I paste :arg1 into :arg2 element
   */
  public function iPasteIntoElement($arg1, $arg2)
  {
    $base_url = $this->getBaseUrl();
    $base_url = rtrim($base_url);
    $text = $this->fixStepArgument($arg1);
    $element = $arg2;
      $js = <<<JS
(function myFunction() {
  //function content
  var testElement = jQuery("$element");
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    jQuery("$element").text("$text");
    var textInElement = jQuery("$element").text();
    if (textInElement == "$text") {
      return true;
    } else {
      return false;
    }
  }
  })();
JS;

    try {
      $this->iPauseSeconds(2);
      $driver = $this->getSession()->getDriver();
      $textInElement = $driver->evaluateScript($js);
      if ($textInElement) {
        $this->assertSession()->pageTextContains($this->fixStepArgument($text));
      } else {
        $this->assertSession()->pageTextContains($this->fixStepArgument($text . ' : ' . $element));
      }
    }
    catch (Exception $e) {
    }
  }

  /**
   * Helper function for I should see :text in xpath :arg2.
   */
  private function xpathElementContainsTextHelper($maxTryNum, $text, $xpath) {
    $element = $xpath;
    $text=$this->fixStepArgument($text);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementTextContains('xpath', $element, $text);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        $e_msg = '';
        $current_url = $this->getSession()->getCurrentUrl();
        $pauseFlag=0;
        if ($e instanceof WebDriver\Exception\StaleElementReference || $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $e_msg = "After $count seconds, the \"$element\" element was not found on page. Using url " . $current_url;
        } else if ($e instanceof Behat\Mink\Exception\ResponseTextException) {
          $e_msg = "After $count seconds, \"$text\" or \"$element\" was not found on page. ResponseTextException Using url " . $current_url;
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $e_msg = "After $count seconds, \"$text\" or \"$element\" was not found on page. FatalThrowableError Using url " . $current_url;
        } else {
          $e_msg = "After $count seconds, \"$text\" or \"$element\" was not found on page. Exception type (other) Using url " . $current_url;
        }
        if ($count == 5) {
          $driver->executeScript("javascript:try { window.scrollTo(0,0); } catch (err) {console.log('force a reset of scrolling.');}");
        }
        if ($count >= ($maxTryNum -1)) {
          $pauseFlag=1; // Enough time spent.
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        if ($count == 0) {
          $this->iPauseSeconds(1);
        }
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that page contains specified text
   * Example: Then I should see "Who is the Batman?" in the xpath "xpath"
   * Example: And I should see "Who is the Batman?" in the xpath "xpath"
   *
   * @Then I should see :arg1 in the xpath :arg2
   */
  public function iShouldSeeInTheXpath($arg1, $arg2)
  {
    $this->xpathElementContainsTextHelper(14, $arg1, $arg2);
  }

  /**
   * Helper function for I should see :text
   */
  private function pageContainsTextHelper($maxTryNum, $text) {
    $text=$this->fixStepArgument($text);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->pageTextContains($text);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($count == 5) {
          $driver->executeScript("javascript:try { window.scrollTo(0,0); } catch (err) {console.log('force a reset of scrolling.');}");
        }
        if ($e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $this->iPauseSeconds(1);
          $pauseFlag=1;
        } else if ($e instanceof Behat\Mink\Exception\ResponseTextException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, \"$text\" was not found on page. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            // Stale element reference??  WebDriver\Exception\StaleElementReference
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, \"$text\" was not found on page. Using url " . $current_url;
	    $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that page contains specified text
   * Example: Then I should see "Who is the Batman?"
   * Example: And I should see "Who is the Batman?"
   *
   * @Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)"$/
   */
  public function assertPageContainsText($text, $threshold = 14)
  {
    $this->pageContainsTextHelper($threshold, $text);
  }

  /**
   * Helper function for I should not see :text
   */
  private function pageTextNotContainsHelper($maxTryNum, $text) {
    $text=$this->fixStepArgument($text);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->pageTextNotContains($text);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $this->iPauseSeconds(1);
          $pauseFlag=1;
        } else if ($e instanceof Behat\Mink\Exception\ResponseTextException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, \"$text\" was found on page but not expected. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          //Find out what type of exception this is?.
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, \"$text\" was found on page but not expected. Using url " . $current_url;
	  $pauseFlag=1; // Enough time spent.
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that page doesn't contain specified text
   * Example: Then I should not see "Batman is Bruce Wayne"
   * Example: And I should not see "Batman is Bruce Wayne"
   *
   * @Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)"$/
   */
  public function assertPageNotContainsText($text)
  {
    $this->pageTextNotContainsHelper(14,$text);
  }

  /**
   * Helper function for I should see text matching "pattern"
   *
   */
  private function pageMatchesTextHelper($maxTryNum, $pattern) {
    $pattern=$this->fixStepArgument($pattern);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->pageTextMatches($pattern);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        if ($e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ResponseTextException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, pattern: \"$pattern\" was not found on page but is expected. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          //Find out what type of exception this is?.
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, pattern: \"$pattern\" was not found on page but is expected. Using url " . $current_url;
	  $pauseFlag=1; // Enough time spent.
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }
  /**
   * Checks, that page contains text matching specified pattern
   * Example: Then I should see text matching "Batman, the vigilante"
   * Example: And I should not see "Batman, the vigilante"
   *
   * @Then /^(?:|I )should see text matching (?P<pattern>"(?:[^"]|\\")*")$/
   */
  public function assertPageMatchesText($pattern)
  {
    $this->pageMatchesTextHelper(16, $pattern);
  }

  /**
   * Checks, that page doesn't contain text matching specified pattern
   * Example: Then I should not see text matching "Bruce Wayne, the vigilante"
   * Example: And I should not see "Bruce Wayne, the vigilante"
   *
   * @Then /^(?:|I )should not see text matching (?P<pattern>"(?:[^"]|\\")*")$/
   */
  public function assertPageNotMatchesText($pattern)
  {
    $this->assertSession()->pageTextNotMatches($this->fixStepArgument($pattern));
  }

  /**
   * Checks, that HTML response contains specified string
   * Example: Then the response should contain "Batman is the hero Gotham deserves."
   * Example: And the response should contain "Batman is the hero Gotham deserves."
   *
   * @Then /^the response should contain "(?P<text>(?:[^"]|\\")*)"$/
   */
  public function assertResponseContains($text)
  {
    try {
      $this->iPauseSeconds(1);
      $this->assertSession()->responseContains($this->fixStepArgument($text));
    }
    catch (Exception $e) {
      $current_url = $this->getSession()->getCurrentUrl();
      $e_msg = "\"$text\" could not be found after 1 second pause in response for " . $current_url;
      throw new Exception($e_msg, $e->getCode(), $e);
    }
  }
 /**
   * Checks, that HTML response doesn't contain specified string
   * Example: Then the response should not contain "Bruce Wayne is a billionaire, play-boy, vigilante."
   * Example: And the response should not contain "Bruce Wayne is a billionaire, play-boy, vigilante."
   *
   * @Then /^the response should not contain "(?P<text>(?:[^"]|\\")*)"$/
   */
  public function assertResponseNotContains($text)
  {
    try {
      $this->iPauseSeconds(2);
      $this->assertSession()->responseNotContains($this->fixStepArgument($text));
    }
    catch (Exception $e) {
      $current_url = $this->getSession()->getCurrentUrl();
      $e_msg = "After 1 second pause \"$text\" was found in response for url " . $current_url;
      throw new Exception($e_msg, $e->getCode(), $e);
    }
  }

  /**
   * Checks, that element with specified CSS contains specified text
   * Example: Then I should see "Batman" in the "heroes_list" element
   * Example: And I should see "Batman" in the "heroes_list" element
   *
   * @Then /^(?:|I )should see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
   */
  public function assertElementContainsText($element, $text)
  {
    $this->elementContainsTextHelper(14, $element, $text);
  }

  /**
   * Helper function for element contains text
   */
  private function elementContainsTextHelper($maxTryNum, $element, $text) {
    $text=$this->fixStepArgument($text);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementTextContains('css', $element, $text);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, the \"$element\" element was not found on page. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Mink\Exception\ElementTextException) {
          $pauseFlag=0;
          if ($count == ($maxTryNum-1)) {
            $count=$maxTryNum+1;
            $pauseFlag=1;
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, $text was not found in \"$element\" on url " . $current_url;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          $pauseFlag=0;
          if ($count == ($maxTryNum-1)) {
            $count=$maxTryNum+1;
            $pauseFlag=1;
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, $text was not found in \"$element\" on url " . $current_url;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
      }
      if (!$pauseFlag) {
        if ($count == 0) {
          $this->iPauseSeconds(1);
        }
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that nth li element in the breadcrumb contains specified text
   * Example: Then I should see "Batman" in sosaa breadcrumb number 1
   * Example: And I should see "Batman" in sosaa breadcrumb number 1
   *
   * @Then I should see :arg1 in sosaa breadcrumb :arg2
   */
  public function iShouldSeeInSosaaBreadcrumb($arg1, $arg2)
  {
    $this->iShouldSeeInLiElementInsideTheElement($arg1, $arg2, 'ol.breadcrumb');
  }

  /**
   * Checks, that nth li element in the breadcrumb contains specified text
   * Example: Then I should see "Batman" in the sosaa breadcrumb number 1
   * Example: And I should see "Batman" in the sosaa breadcrumb number 1
   *
   * @Then I should see :arg1 in the sosaa breadcrumb :arg2
   */
  public function iShouldSeeInTheSosaaBreadcrumb($arg1, $arg2)
  {
    $this->iShouldSeeInLiElementInsideTheElement($arg1, $arg2, 'ol.breadcrumb');
  }

  /**
   * Checks, that nth li element in the breadcrumb contains specified text
   * Example: Then I should see "Batman" in pdata breadcrumb number 1
   * Example: And I should see "Batman" in pdata breadcrumb number 1
   *
   * @Then I should see :arg1 in pdata breadcrumb :arg2
   */
  public function iShouldSeeInPdataBreadcrumb($arg1, $arg2)
  {
    $this->iShouldSeeInLiElementInsideTheElement($arg1, $arg2, 'ol.breadcrumbs');
  }

  /**
   * Checks, that nth li element in the breadcrumb contains specified text
   * Example: Then I should see "Batman" in the pdata breadcrumb
   * Example: And I should see "Batman" in the pdata breadcrumb
   *
   * @Then I should see :arg1 in the pdata breadcrumb :arg2
   */
  public function iShouldSeeInThePdataBreadcrumb($arg1, $arg2)
  {
    $this->iShouldSeeInLiElementInsideTheElement($arg1, $arg2, 'ol.breadcrumbs');
  }

  /**
   * Checks, that form element with specified label and type is visible on page.
   *
   * @Then /^(?:|I )should see a "(?P<label>[^"]*)" "(?P<type>[^"]*)" form element$/
   * @Then /^(?:|I )should see an? "(?P<label>[^"]*)" "(?P<type>[^"]*)" form element$/
   */
  public function assertTypedFormElementOnPage($label, $type) {
    $container = $this->getSession()->getPage();
    $pattern = '/(^| )form-type-' . preg_quote($type). '($| )/';
    $label_nodes = $container->findAll('css', '.form-item label');
    foreach ($label_nodes as $label_node) {
      // Note: getText() will return an empty string when using Selenium2D. This
      // is ok since it will cause a failed step.
      if ($label_node->getText() === $label
        && preg_match($pattern, $label_node->getParent()->getAttribute('class'))
        && $label_node->isVisible()) {
        return;
      }
    }
    throw new \Behat\Mink\Exception\ElementNotFoundException($this->getSession(), $type . ' form item', 'label', $label);
  }

  /**
   * Checks, that nth :arg2 element in the :arg4 element contains specified text
   * Example: Then I should see "Batman" in "option" element "1" inside the "#some-id" element
   * Example: And I should see "Batman" in "option" element "1" inside the "#some-id" element
   *
   * @Then I should see :arg1 in :arg2 element :arg3 inside the :arg4 element
   */
# Then I should see "None" in "option" element 0 inside the "#edit-combine-select-auth-prov-terr-user" element
  public function iShouldSeeInElementInsideTheElement($arg1, $arg2, $arg3, $arg4)
  {
    $debug0='';
    $text=$arg1;
    $childrenElements=$this->fixStepArgument($arg2);
    $num=intval($arg3);
    $element=$arg4;
    $element=$this->fixStepArgument($element);
    $text=$this->fixStepArgument($text);
    $outerElement = $this->getSession()->getPage()->find('css', $element);

    if (!isset($outerElement)) {
      $this->iPauseSeconds(3);
    }
    $childElements = $this->getSession()->getPage()->findAll('css', $element . ' ' . $childrenElements);
    $count = 0;
    $found = FALSE;
    $textGotten = '';
    foreach($childElements as $childElement) {
      if ($count == $num) {
        $textGotten = $childElement->getText();
      }
      $debug0 = $childElement->getText() . ':' . $debug0;
      $count++;
    }
    $count1= $count;
    $count = 0;
    if (stripos($textGotten, $text) !== FALSE) {
      $this->assertSession()->pageTextContains($text);
    } else {
      $current_url = $this->getSession()->getCurrentUrl();
      throw new Exception("No match between :textGotten=$textGotten and :text=$text I should see :arg1 in a $childrenElements tag) inside the \"$element\" element $text not found in $childrenElements element $num of \"$element\" on page with url $current_url :debug0: $debug0");
    }
  }

  /**
   * Checks, that nth li element in the breadcrumb contains specified text
   * Example: Then I should see "Batman" in li element "1" inside the "ol.breadcrumbs" element
   * Example: And I should see "Batman" in li element "1" inside the "ol.breadcrumbs" element
   *
   * @Then I should see :arg1 in li element :arg2 inside the :arg3 element
   */
  public function iShouldSeeInLiElementInsideTheElement($arg1, $arg2, $arg3)
  {
    $text=$arg1;
    $num=$arg2;
    $element=$arg3;
    $element=$this->fixStepArgument($element);
    $text=$this->fixStepArgument($text);
    $outerElement = $this->getSession()->getPage()->find('css', $element);

    if (!isset($outerElement)) {
      $this->iPauseSeconds(3);
    }
    $liElements = $this->getSession()->getPage()->findAll('css', $element . ' li');
    $count = 0;
    $found = FALSE;
    $textGotten = '';
    foreach($liElements as $liElement) {
      if ($count == $num) {
        $textGotten = $liElement->getText();
      }
      $debug0 = $liElement->getText() . ':' . $debug0;
      $count++;
    }
    $count1= $count;
    $count = 0;
    if (stripos($textGotten, $text) !== FALSE) {
      $this->assertSession()->pageTextContains($text);
      //$this->assertSession()->elementTextContains('css', $element, $text);
    } else {
      $aElements = $this->getSession()->getPage()->findAll('css', $element . ' a');
      $count = 0;
      foreach($aElements as $aElement) {
        if ($count == $num) {
          $textGotten = $aElement->getText();
        }
        $debug1 = $aElement->getText() . ':' . $debug1;
        $count++;
      }
      if (stripos($textGotten, $text) !== FALSE) {
        $this->assertSession()->pageTextContains($text);
      } else {
        $current_url = $this->getSession()->getCurrentUrl();
        throw new Exception("No match between :textGotten=$textGotten and :text=$text I should see :arg1 in (a tag) element :arg2 inside the \"$element\" li a element $text not found in li element $num of \"$element\" on page with url $current_url");
      }
    }
  }

  /**
   * Helper function for element does not contain text
   */
  private function elementNotContainsTextHelper($maxTryNum, $element, $text) {
    $text=$this->fixStepArgument($text);
    $count = 0;
    while($count < $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementTextNotContains('css', $element, $this->fixStepArgument($text));
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, the \"$element\" element was not found on page. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Mink\Exception\ElementTextException) {
          $pauseFlag=0;
          if ($count == ($maxTryNum-1)) {
            $count=$maxTryNum+1;
            $pauseFlag=1;
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, $text was found in \"$element\" on url " . $current_url;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          // Find out what this exception is.
          $pauseFlag=1;
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, $text was found in \"$element\" on url " . $current_url;
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }
  /**
   * Checks, that element with specified CSS doesn't contain specified text
   * Example: Then I should not see "Bruce Wayne" in the "heroes_alter_egos" element
   * Example: And I should not see "Bruce Wayne" in the "heroes_alter_egos" element
   *
   * @Then /^(?:|I )should not see "(?P<text>(?:[^"]|\\")*)" in the "(?P<element>[^"]*)" element$/
   */
  public function assertElementNotContainsText($element, $text)
  {
    $this->elementNotContainsTextHelper(14, $element, $text);
  }

  /**
   * Helper function for the element element should contain value.
   */
  private function elementContainsHelper($maxTryNum, $element, $value) {
    $value=$this->fixStepArgument($value);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementContains('css', $element, $value);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, the \"$element\" element was not found on page. Using url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Mink\Exception\ElementTextException) {
          $pauseFlag=0;
          if ($count == ($maxTryNum-1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, \"$element\" does not contain \"$value\" n url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementHtmlException) {
          $pauseFlag=0;
        } else {
          // Find out what this exception is.
          $pauseFlag=1;
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, \"$element\" does not contain \"$value\" n url " . $current_url;
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that element with specified CSS contains specified HTML
   * Example: Then the "body" element should contain "style=\"color:black;\""
   * Example: And the "body" element should contain "style=\"color:black;\""
   *
   * @Then /^the "(?P<element>[^"]*)" element should contain "(?P<value>(?:[^"]|\\")*)"$/
   */
  public function assertElementContains($element, $value)
  {
    $this->elementContainsHelper(14, $element, $value);
  }

  /**
   * Helper function for Then the element element should not contain value.
   */
  private function elementNotContainsHelper($maxTryNum, $element, $value) {
    $value=$this->fixStepArgument($value);
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementNotContains('css', $element, $value);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, \"$element\" does contain \"$value\" n url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          // Find out what this exception is.
          $pauseFlag=1;
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, \"$element\" does contain \"$value\" n url " . $current_url;
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that element with specified CSS doesn't contain specified HTML
   * Example: Then the "body" element should not contain "style=\"color:black;\""
   * Example: And the "body" element should not contain "style=\"color:black;\""
   *
   * @Then /^the "(?P<element>[^"]*)" element should not contain "(?P<value>(?:[^"]|\\")*)"$/
   */
  public function assertElementNotContains($element, $value)
  {
    $this->elementNotContainsHelper(14, $element, $value);
  }

  /**
   * Helper function for making sure the element is on the page.
   */
  private function elementOnPageHelper($maxTryNum, $element) {
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementExists('css', $element);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, \"$element\" element was not found on url " . $current_url;
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
        } else {
          $pauseFlag=1;
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, element matching css \"$element\" was not found on page. Using url " . $current_url;
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that element with specified CSS exists on page
   * Example: Then I should see a "body" element
   * Example: And I should see a "body" element
   *
   * @Then /^(?:|I )should see an? "(?P<element>[^"]*)" element$/
   */
  public function assertElementOnPage($element)
  {
    $this->elementOnPageHelper(14, $element);
  }

  /**
   * Helper function for making sure the element is not on the page.
   */
  private function elementNotOnPageHelper($maxTryNum, $element) {
    $count = 0;
    while($count <= $maxTryNum) {
      $pauseFlag=0;
      try {
        $this->assertSession()->elementNotExists('css', $element);
        $count=$maxTryNum+1;
        $pauseFlag=1;
      } catch (Exception $e) {
        $driver = $this->getSession()->getDriver();
        $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
        if ($e instanceof WebDriver\Exception\StaleElementReference ||
          $e instanceof WebDriver\Exception\UnexpectedAlertOpen) {
          // Stale element reference, page is being rendered and we're too fast.
	  // EnexpectedAlertOpen, we didnt wait long enough for an Ajax call and it is still processing.
	  // Could be of context because we did something else.
          $pauseFlag=0;
        } else if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $this->assertSession()->elementNotExists('css', $element);
            $count=$maxTryNum+1;
            $pauseFlag=1;// Enough waiting
	  }
        } else if ($e instanceof Behat\Testwork\Call\Exception\FatalThrowableError) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $this->assertSession()->elementNotExists('css', $element);
            $count=$maxTryNum+1;
            $pauseFlag=1;// Enough waiting
	  }
        } else {
          $pauseFlag=1;
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, \"$element\" css was not found in page content for url " . $current_url;
          $count=$maxTryNum+1;
          throw new Exception($e_msg, $e->getCode(), $e);
        }
      }
      if (!$pauseFlag) {
        $this->iPauseSeconds(1);
      }
      $count++;
    }
  }

  /**
   * Checks, that element with specified CSS doesn't exist on page
   * Example: Then I should not see a "canvas" element
   * Example: And I should not see a "canvas" element
   *
   * @Then /^(?:|I )should not see an? "(?P<element>[^"]*)" element$/
   */
  public function assertElementNotOnPage($element)
  {
    $this->elementNotOnPageHelper(14, $element);
  }

  /**
   * Checks, that form field with specified id|name|label|value has specified value
   * Example: Then the "username" field should contain "bwayne"
   * Example: And the "username" field should contain "bwayne"
   *
   * @Then /^the "(?P<field>(?:[^"]|\\")*)" field should contain "(?P<value>(?:[^"]|\\")*)"$/
   */
  public function assertFieldContains($field, $value)
  {
    $field = $this->fixStepArgument($field);
    $value = $this->fixStepArgument($value);
    try {
      $this->assertSession()->fieldValueEquals($field, $value);
    }
    catch (Exception $e) {
      $driver = $this->getSession()->getDriver();
      $driver->executeScript("javascript:try { window.scrollBy(0,250); } catch (err) {console.log('force the checkbox prep.');}");
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->fieldValueEquals($field, $value);
      }
      catch (Exception $e) {
        $this->iPauseSeconds(4);
        $this->assertSession()->fieldValueEquals($field, $value);
      }
    }
  }

  /**
   * Checks, that form field with specified id|name|label|value doesn't have specified value
   * Example: Then the "username" field should not contain "batman"
   * Example: And the "username" field should not contain "batman"
   *
   * @Then /^the "(?P<field>(?:[^"]|\\")*)" field should not contain "(?P<value>(?:[^"]|\\")*)"$/
   */
  public function assertFieldNotContains($field, $value)
  {
    $field = $this->fixStepArgument($field);
    $value = $this->fixStepArgument($value);
    try {
      $this->assertSession()->fieldValueNotEquals($field, $value);
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->fieldValueNotEquals($field, $value);
      }
      catch (Exception $e) {
        $this->iPauseSeconds(4);
        $this->assertSession()->fieldValueNotEquals($field, $value);
      }
    }
  }

  /**
   * Checks, that (?P<num>\d+) CSS elements exist on the page
   * Example: Then I should see 5 "div" elements
   * Example: And I should see 5 "div" elements
   *
   * @Then /^(?:|I )should see (?P<num>\d+) "(?P<element>[^"]*)" elements?$/
   */
  public function assertNumElements($num, $element)
  {
    try {
      $this->assertSession()->elementsCount('css', $element, intval($num));
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->elementsCount('css', $element, intval($num));
      }
      catch (Exception $e) {
        $this->iPauseSeconds(4);
        $this->assertSession()->elementsCount('css', $element, intval($num));
      }
    }
  }

 /**
  * Checks, that checkbox with specified id|name|label|value is checked
  * Example: Then the "remember_me" checkbox should be checked
  * Example: And the "remember_me" checkbox is checked
  *
  * @Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should be checked$/
  * @Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox is checked$/
  * @Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" (?:is|should be) checked$/
  */
  public function assertCheckboxChecked($checkbox)
  {
    $checkbox = $this->fixStepArgument($checkbox);
    if (strpos($checkbox, '#') === 0) {
      $select = substr($checkbox, 1);
    }
    try {
      $this->assertSession()->checkboxChecked($checkbox);
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->checkboxChecked($checkbox);
      }
      catch (Exception $e) {
        $this->iPauseSeconds(4);
        $this->assertSession()->checkboxChecked($checkbox);
      }
    }
  }

 /**
  * Checks, that checkbox with specified id|name|label|value is unchecked
  * Example: Then the "newsletter" checkbox should be unchecked
  * Example: Then the "newsletter" checkbox should not be checked
  * Example: And the "newsletter" checkbox is unchecked
  *
  * @Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox should (?:be unchecked|not be checked)$/
  * @Then /^the "(?P<checkbox>(?:[^"]|\\")*)" checkbox is (?:unchecked|not checked)$/
  * @Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" should (?:be unchecked|not be checked)$/
  * @Then /^the checkbox "(?P<checkbox>(?:[^"]|\\")*)" is (?:unchecked|not checked)$/
  */
  public function assertCheckboxNotChecked($checkbox)
  {
    try {
      $this->assertSession()->checkboxNotChecked($this->fixStepArgument($checkbox));
    }
    catch (Exception $e) {
      try {
        $this->iPauseSeconds(1);
        $this->assertSession()->checkboxNotChecked($this->fixStepArgument($checkbox));
      }
      catch (Exception $e) {
        $this->iPauseSeconds(4);
        $this->assertSession()->checkboxNotChecked($this->fixStepArgument($checkbox));
      }
    }
  }

 /**
  * Prints current URL to console.
  * Example: Then print current URL
  * Example: And print current URL
  *
  * @Then /^print current URL$/
  */
  public function printCurrentUrl()
  {
    echo $this->getSession()->getCurrentUrl();
  }
 /**
  * Prints last response to console
  * Example: Then print last response
  * Example: And print last response
  *
  * @Then /^print last response$/
  */
  public function printLastResponse()
  {
    echo (
      $this->getSession()->getCurrentUrl()."\n\n".
      $this->getSession()->getPage()->getContent()
    );
  }
 /**
  * Opens last response content in browser
  * Example: Then show last response
  * Example: And show last response
  *
  * @Then /^show last response$/
  */
  public function showLastResponse()
  {
    if (null === $this->getMinkParameter('show_cmd')) {
      throw new \RuntimeException('Set "show_cmd" parameter in behat.yml to be able to open page in browser (ex.: "show_cmd: firefox %s")');
    }
   $filename = rtrim($this->getMinkParameter('show_tmp_dir'), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.uniqid().'.html';
    file_put_contents($filename, $this->getSession()->getPage()->getContent());
    system(sprintf($this->getMinkParameter('show_cmd'), escapeshellarg($filename)));
  }

 /**
  * Returns fixed step argument (with \\" replaced back to ")
  *
  * @param string $argument
  *
  * @return string
  */
  protected function fixStepArgument($argument)
  {
    return str_replace('\\"', '"', $argument);
  }

 /**
  * @Then press enter from :arg1 element
  */
  public function pressEnterFromElement()
  {
    // Needs testing, not sure if this method works or not.
    $argTest = $this->fixStepArgument($arg1);
    $this->getSession()->getPage()->find('css', $argTest)->keyPress(13, null);
  }

 /**
  * @Given I write :text into :field
  */
  public function iWriteTextIntoField($text, $field)
  {
    $session = $this->getSession();
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('named_exact', array('field', $field)) // input field named $field
    );
    $element->postValue(array('value' => array($text)));
    //$element->postValue(array('value' => array("\r\n"))); // hits enter.

    // Based off of:
    /*$this->getSession()
      ->getDriver()
      ->getWebDriverSession()
      ->element('xpath', $this->getSession()->getSelectorsHandler()->selectorToXpath('named_exact', array('field', $field))
      ->postValue(['value' => [$text]]));*/
    //->postValue(['value' => ["\r\n"]]); // hits enter.
  }

  /*
   * @Then I confirm the popup
   */
  public function iConfirmThePopup()
  {
    $this->getDriver()->getWebDriverSession()->accept_alert();
  }

  /*
   * @Then I confirm the popup and fail the test
   */
  public function iConfirmThePopupAndFailTheTest()
  {
    try {
      $this->getDriver()->getWebDriverSession()->accept_alert();
    } catch (Exception $e) {
      $current_url = $this->getSession()->getCurrentUrl();
      $e_msg = "An alert popup was clicked. Fail this test on purpose on url " . $current_url;
      throw new Exception($e_msg/*, $e->getCode(), $e*/);
    }
  }

 /**
  * Selector to element with attribute ensure it has value.
  * Example: Then I use "seclector" for "attribute" with "value" equaling
  *
  * @When I search for :arg1 on :arg2 using selector :arg3
  */
  public function iSearchForOnUsingSelector($value, $attribute, $selector) {
    $this->elementAttributeValueHelper(14, $selector, $attribute, $value);
  }


  /**
   * Helper function for the select_this element should contain attribute with value.
   */
  private function elementAttributeValueHelper($maxTryNum, $select_this, $attribute, $value) {
    $count = 0;
    $value = $this->fixStepArgument($value);
    $select_this = $this->fixStepArgument($select_this);
    $attribute = $this->fixStepArgument($attribute);
    $select_this = trim($select_this);
    $attribute = trim($attribute);
    $value = trim($value);
    while($count <= $maxTryNum) {
      $pauseFlag=0;
$js = <<<JS
(function myFunction() {
  //function content
  var testElement = jQuery("$select_this").first();
  if (typeof(testElement) == "undefined") {
    return false;
  } else {
    if (jQuery(testElement).attr("$attribute") == "$value") {
      return true;
    }
    return false;
  }
  })();
JS;

      try {
        $this->iPauseSeconds(1);
        $driver = $this->getSession()->getDriver();
        $valueInAttribute = $driver->evaluateScript("return $js");
        if (!$valueInAttribute) {
          throw new \Behat\Mink\Exception\ElementNotFoundException($this->getSession(), "$value not found for $attribute using selector: $select_this .", 'attribute', $attribute);
        }
        $count=$maxTryNum+1;
        return true;
      }
      catch (Exception $e) {
        if ($e instanceof Behat\Mink\Exception\ElementNotFoundException) {
          $pauseFlag=0;
          if ($count >= ($maxTryNum -1)) {
            $current_url = $this->getSession()->getCurrentUrl();
            $e_msg = "After $count seconds, the \"$select_this\" element was not found on page. Using url " . $current_url . " ";
            $pauseFlag=1; // Enough time spent.
            $count=$maxTryNum+1;
            throw new Behat\Mink\Exception\ElementNotFoundException($this->getSession(), $e_msg . " $value not found for $attribute using selector: $select_this .", 'attribute', $attribute);
          }
        } else {
          // Find out what this exception is.
          $pauseFlag=0;
          $current_url = $this->getSession()->getCurrentUrl();
          $e_msg = "After $count seconds, \"$select_this\" does not have attribute \"$attribute\" with value " . $value;
          $count=$maxTryNum+1;
          if ($count >= ($maxTryNum -1)) {
            $pauseFlag=1;
            throw new Exception($e_msg, $e->getCode(), $e);
          }
        }
        if (!$pauseFlag) {
          $this->iPauseSeconds(1);
        }
        $count++;
      }
    }
    if ($count >= $maxTryNum) {
      $e_msg = "After $count seconds, \"$select_this\" does not have attribute \"$attribute\" with value " . $value;
      throw new Exception($e_msg, 0, E_ERROR, NULL);
    }
  }

}

