Feature: Create new Internal Page content and publish content - role Creator

  Scenario: Logged in users should be able to create and manage content appropriate to their roles - role Creator

  # Creator logs into Agrisource
    Given I am on "/user/logout"
    Then I pause 2 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    Then I fill in the following:
          | edit-name | agrisourcebehat_creator |
          | edit-pass | PasswordTestLongMotDePasse1 |
    And I click the "#edit-submit" button
    Then I pause 2 seconds
    And I should see "Moderation Dashboard" in the "h1" element
    # If we do not see "Moderation Dashboard" it is because at this point it could be due to the account does not having the necessary role as any privileged role will have access to the Moderation Dashboard.
    And I should not see "Error message"
  
  # Creator role creates a new Internal Page  
    # Verify that we are on the Create Internal Page form and that an empty form submission generates validation errors
    Given I am on "/node/add/page"
    And I pause 7 seconds
    Then I should see "Create Internal page" in the "h1" element
    When I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "Title (English) field is required."
    And I should see "Title (French) field is required."

    # Add input or make selections for each field
        # IMPORTANT STEP: Fix for behat/selenium unable to beat html5 validation for wysiwyg ckeditor fields.
    Then I ask behat to disable html5 form validation for the "edit-body-0-value" field
    And I fill in "edit-title-0-value" with "Test Internal Page title EN by agrisourcebehat_creator"   
    Then I ask behat to disable html5 form validation for the "edit-body-etuf-fr-0-value" field
    And I fill in "edit-title-etuf-fr-0-value" with "Test Internal Page title FR by agrisourcebehat_creator"
    
    Then I fill in the following:
      | edit-revision-log-0-value | revision log Internal Page test by agrisourcecreator@yopmail.com |
      | edit-field-tracking-number-0-value | Test-tracking-number-1234 |

    # Verify functionality of the button ... Preview
    When I click the "#edit-preview" button
    And I pause 5 seconds
    Then I should see "Back to content editing"
    Then I should see "Test Internal Page title EN by agrisourcebehat_creator" in the "h1" element
    And I should not see "Error message"
    
    # Verify functionality of the link ... Return to form in progress
    When I click the "#edit-backlink" button
    And I pause 5 seconds
    Then I should not see "Back to content editing"
    
    # Verify functionality of the button ... Submit
    When I click the "#edit-submit" button
    And I pause 5 seconds

    # Successful Submit should display a Confirmation message and now Viewing the Internal Page
    Then I should see "has been created."
    And I should not see "Undefined variable:"

    # Verify that the language switcher works to FR and back to EN and also that breadcrumbs are correct in both languages
    Then I should see "Test Internal Page title EN by agrisourcebehat_creator" in the "h1" element
    And I should see "Home" in the "nav#wb-bc" element
    And I should see "Test Internal Page title EN by agrisourcebehat_creator" in the "nav#wb-bc" element
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Test Internal Page title FR by agrisourcebehat_creator" in the "h1" element
    And I should see "Accueil" in the "nav#wb-bc" element
    And I should see "Test Internal Page title FR by agrisourcebehat_creator" in the "nav#wb-bc" element
    And I should not see "Message d'erreur"
    When I click the "a.language-link" button
    And I pause 5 seconds
    Then I should see "Test Internal Page title EN by agrisourcebehat_creator" in the "h1" element
    And I should not see "Error message"
