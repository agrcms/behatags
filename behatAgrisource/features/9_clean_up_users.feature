Feature: Clean up the leftover users created during earlier behat test scenarios

Scenario: All users created during a full run of behat tests should be deleted.

    # First logout of the system, then login using admin level account
    Given I am on "/user/logout"
    Then I pause 4 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button

    # Delete User(s)
    Given I am on "/admin/people/behat"
    And I pause 2 seconds
    Then I should see "People" in the "h1" element
    And I fill in "#edit-user" with "agrisourcebehat_cspub"
    And I click the "#edit-submit-user-admin-people" button
    And I pause 5 seconds
    Then I should see "agrisourcebehat_cspub edit link"
    When I click "agrisourcebehat_cspub edit link"
    And I pause 5 seconds
# workaround needed for cloud environments that have password restriction mode enabled. (the 'live' config)    
    Then I fill in the following:
	    | edit-pass-pass1 | PasswordTestLongMotDePasse1 |
	    | edit-pass-pass2 | PasswordTestLongMotDePasse1 |
    Then I click the "#edit-delete" button
    And I pause 3 seconds
    Then I select option "user_cancel_delete" from "user_cancel_method"
    Then I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "agrisourcebehat_cspub has been deleted"
#    And I should not see "Error message"
    
    Then I fill in "#edit-user" with "agrisourcebehat_ccpub"
    And I click the "#edit-submit-user-admin-people" button
    And I pause 5 seconds
    Then I should see "agrisourcebehat_ccpub edit link"
    When I click "agrisourcebehat_ccpub edit link"
    And I pause 5 seconds
# workaround needed for cloud environments that have password restriction mode enabled. (the 'live' config)    
    Then I fill in the following:
	    | edit-pass-pass1 | PasswordTestLongMotDePasse1 |
	    | edit-pass-pass2 | PasswordTestLongMotDePasse1 |
    Then I click the "#edit-delete" button
    And I pause 3 seconds
    Then I select option "user_cancel_delete" from "user_cancel_method"
    Then I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "agrisourcebehat_ccpub has been deleted"
    
    Then I fill in "#edit-user" with "agrisourcebehat_creator"
    And I click the "#edit-submit-user-admin-people" button
    And I pause 5 seconds
    Then I should see "agrisourcebehat_creator edit link"
    When I click "agrisourcebehat_creator edit link"
    And I pause 5 seconds
# workaround needed for cloud environments that have password restriction mode enabled. (the 'live' config)    
    Then I fill in the following:
	    | edit-pass-pass1 | PasswordTestLongMotDePasse1 |
	    | edit-pass-pass2 | PasswordTestLongMotDePasse1 |
    Then I click the "#edit-delete" button
    And I pause 3 seconds
    Then I select option "user_cancel_delete" from "user_cancel_method"
    Then I click the "#edit-submit" button
    And I pause 3 seconds
    Then I should see "agrisourcebehat_creator has been deleted"

    # Final logout of the system
    Given I am on "/user/logout"
    And I pause 4 seconds
    
