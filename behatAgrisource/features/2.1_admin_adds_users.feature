Feature: Add users required by the next sets of tests

  Scenario: Log in ( admin ), and create users.

# First logout of the system, then login using admin level account
    Given I am on "/user/logout"
    And I pause 2 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    And I should not see "Error message"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button
    And I pause 4 seconds
    
#create creator
    Then I should see "Moderation Dashboard" in the "h1" element
    # If we do not see "Moderation Dashboard" it is because at this point it could be due to the account does not having the necessary role as any privileged role will have access to the Moderation Dashboard.
    Given I am on "/admin/people"
    Then I should see "Add user"
    Then I fill in "edit-user" with "agrisourcebehat_"
    And I click the "#edit-submit-user-admin-people" button
    And I pause 4 seconds
    Then I should not see "agrisourcebehat_creator"
    Then I click the "a.button--action.button--primary" button
    #Given I am on "/admin/people/create"
    And I pause 3 seconds
    Then I should see "email addresses and usernames must be unique"
	When I fill in the following:
	    | edit-pass-pass1 | PasswordTestLongMotDePasse1 |
	    | edit-pass-pass2 | PasswordTestLongMotDePasse1 |
	    | edit-field-first-name-0-value | agrisourcebehat |
	    | edit-field-last-name-0-value | creator |
	    | edit-mail | agrisourcebehat_creator@yopmail.com |
	    | edit-name | agrisourcebehat_creator |
	Then I should see "yes"
    Then I force check "Creator"
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "Created a new user account for"
    And I should see "agrisourcebehat_creator"

#create ccpub
    Then I fill in "edit-field-first-name-0-value" with "agrisourcebehat"
	When I fill in the following:
	    | edit-pass-pass1 | PasswordTestLongMotDePasse1 |
	    | edit-pass-pass2 | PasswordTestLongMotDePasse1 |
        | edit-field-first-name-0-value | agrisourcebehat |
	    | edit-field-last-name-0-value | ccpub |
        | edit-mail | agrisourcebehat_ccpub@yopmail.com |
	    | edit-name | agrisourcebehat_ccpub |
	Then I should see "yes"
    Then I force check "ccpub"
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "Created a new user account for"
    Then I should see "agrisourcebehat_ccpub"

#create cspub
    Then I fill in "edit-field-first-name-0-value" with "agrisourcebehat"
	When I fill in the following:
	    | edit-field-first-name-0-value | agrisourcebehat |
	    | edit-field-last-name-0-value | cspub |
	    | edit-mail | agrisourcebehat_cspub@yopmail.com |
	    | edit-name | agrisourcebehat_cspub |
	    | edit-pass-pass1 | PasswordTestLongMotDePasse1 |
	    | edit-pass-pass2 | PasswordTestLongMotDePasse1 |
	Then I should see "yes"
    Then I force check "cspub"
    Then I click the "#edit-submit" button
    And I pause 5 seconds
    Then I should see "Created a new user account for"
    Then I should see "agrisourcebehat_cspub"
#    And I should not see "Error message"

# Post-test clean-up

    # First logout of the system, then login using admin level account
#    Given I am on "/user/logout"
#    Then I pause 4 seconds
#
#    Given I am on "/user/login"
#    Then I should see "Log in"
#    When I fill in "edit-name" with "agrisourcebehat_adm"
#    And I fill in "edit-pass" with "PasswordTestLongMotDePasse1"
#    And I click the "#edit-submit" button
#
#    # Delete User(s)
#    Given I am on "/admin/people/behat"
#    And I pause 2 seconds
#    Then I should see "People" in the "h1" element
#    And I fill in "#edit-user" with "agrisourcebehat_cspub"
#    And I click the "#edit-submit-user-admin-people" button
#    And I pause 5 seconds
#    Then I should see "agrisourcebehat_cspub edit link"
#    When I click "agrisourcebehat_cspub edit link"
#    And I pause 5 seconds
#    Then I click the "#edit-delete" button
#    And I pause 3 seconds
#    Then I select option "user_cancel_delete" from "user_cancel_method"
#    Then I click the "#edit-submit" button
#    And I pause 3 seconds
#    Then I should see "agrisourcebehat_cspub has been deleted"
#    
#    Then I fill in "#edit-user" with "agrisourcebehat_ccpub"
#    And I click the "#edit-submit-user-admin-people" button
#    And I pause 5 seconds
#    Then I should see "agrisourcebehat_ccpub edit link"
#    When I click "agrisourcebehat_ccpub edit link"
#    And I pause 5 seconds
#    Then I click the "#edit-delete" button
#    And I pause 3 seconds
#    Then I select option "user_cancel_delete" from "user_cancel_method"
#    Then I click the "#edit-submit" button
#    And I pause 3 seconds
#    Then I should see "agrisourcebehat_ccpub has been deleted"
#    
#    Then I fill in "#edit-user" with "agrisourcebehat_creator"
#    And I click the "#edit-submit-user-admin-people" button
#    And I pause 5 seconds
#    Then I should see "agrisourcebehat_creator edit link"
#    When I click "agrisourcebehat_creator edit link"
#    And I pause 5 seconds
#    Then I click the "#edit-delete" button
#    And I pause 3 seconds
#    Then I select option "user_cancel_delete" from "user_cancel_method"
#    Then I click the "#edit-submit" button
#    And I pause 3 seconds
#    Then I should see "agrisourcebehat_creator has been deleted"
#
#    # Final logout of the system
#    Given I am on "/user/logout"
#    And I pause 4 seconds
 
