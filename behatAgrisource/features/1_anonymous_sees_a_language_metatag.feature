Feature: Anonymous sees several metatag elements in english and french on multiple pages

  Scenario Outline: Anonymous sees expected metatag elements such as language, format, issued in English
  # Verify that on the EN homepage we do see the expected elements
    Given I navigate to "<url>"
    Then I should see "AgriSource" in the "a.navbar-brand" element
    When I search for "<value>" on "<attribute>" using selector "<meta-selector>"

    Examples:
      | url | attribute | value | meta-selector |
      | / | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | / | title | W3CDTF | meta[name='dcterms.issued'] |
      | / | name | dcterms.format | meta[title='gcformat'] |
      | / | name | dcterms.language | meta[title='ISO639-2'] |
      | / | content | eng | meta[title='ISO639-2'] |
      | /news/newswork | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
#      | /news/newswork | title | W3CDTF | meta[name='dcterms.issued'] |
#      | /news/newswork | name | dcterms.format | meta[title='gcformat'] |
      | /news/newswork | name | dcterms.language | meta[title='ISO639-2'] |
      | /news/newswork | content | eng | meta[title='ISO639-2'] |
      | /find-people-and-collaborate | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /find-people-and-collaborate | title | W3CDTF | meta[name='dcterms.issued'] |
      | /find-people-and-collaborate | name | dcterms.format | meta[title='gcformat'] |
      | /find-people-and-collaborate | name | dcterms.language | meta[title='ISO639-2'] |
      | /find-people-and-collaborate | content | eng | meta[title='ISO639-2'] |
      | /branches-and-offices | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /branches-and-offices | title | W3CDTF | meta[name='dcterms.issued'] |
      | /branches-and-offices | name | dcterms.format | meta[title='gcformat'] |
      | /branches-and-offices | name | dcterms.language | meta[title='ISO639-2'] |
      | /branches-and-offices | content | eng | meta[title='ISO639-2'] |
      | /human-resources | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /human-resources | title | W3CDTF | meta[name='dcterms.issued'] |
      | /human-resources | name | dcterms.format | meta[title='gcformat'] |
      | /human-resources | name | dcterms.language | meta[title='ISO639-2'] |
      | /human-resources | content | eng | meta[title='ISO639-2'] |
      | /our-department | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /our-department | title | W3CDTF | meta[name='dcterms.issued'] |
      | /our-department | name | dcterms.format | meta[title='gcformat'] |
      | /our-department | name | dcterms.language | meta[title='ISO639-2'] |
      | /our-department | content | eng | meta[title='ISO639-2'] |
      | /our-department/social-media-resources | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /our-department/social-media-resources | title | W3CDTF | meta[name='dcterms.issued'] |
      | /our-department/social-media-resources | name | dcterms.format | meta[title='gcformat'] |
      | /our-department/social-media-resources | name | dcterms.language | meta[title='ISO639-2'] |
      | /our-department/social-media-resources | content | eng | meta[title='ISO639-2'] |
      | /contact-us | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /contact-us | title | W3CDTF | meta[name='dcterms.issued'] |
      | /contact-us | name | dcterms.format | meta[title='gcformat'] |
      | /contact-us | name | dcterms.language | meta[title='ISO639-2'] |
      | /contact-us | content | eng | meta[title='ISO639-2'] |
      | /home/tools-and-services | content | Agriculture and Agri-Food Canada | meta[name='dcterms.creator'] |
      | /home/tools-and-services | title | W3CDTF | meta[name='dcterms.issued'] |
      | /home/tools-and-services | name | dcterms.format | meta[title='gcformat'] |
      | /home/tools-and-services | name | dcterms.language | meta[title='ISO639-2'] |
      | /home/tools-and-services | content | eng | meta[title='ISO639-2'] |

  Scenario Outline: Anonymous sees expected metatag elements such as language, format, issued en Français
  # Verify that on the FR homepage we do see the expected elements
    Given I navigate to "<url>"
    Then I should see "AgriSource" in the "a.navbar-brand" element
    Then I pause 1 seconds
    Then I click the "a.language-link" button
    Then I should see "English" in the "a.language-link" element
    When I search for "<value>" on "<attribute>" using selector "<meta-selector>"

    Examples:
      | url | attribute | value | meta-selector |
      | / | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | / | title | W3CDTF | meta[name='dcterms.issued'] |
      | / | name | dcterms.format | meta[title='gcformat'] |
      | / | name | dcterms.language | meta[title='ISO639-2'] |
      | / | content | fra | meta[title='ISO639-2'] |
      | /news/newswork | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
#      | /news/newswork | title | W3CDTF | meta[name='dcterms.issued'] |
#      | /news/newswork | name | dcterms.format | meta[title='gcformat'] |
      | /news/newswork | name | dcterms.language | meta[title='ISO639-2'] |
      | /news/newswork | content | fra | meta[title='ISO639-2'] |
      | /find-people-and-collaborate | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /find-people-and-collaborate | title | W3CDTF | meta[name='dcterms.issued'] |
      | /find-people-and-collaborate | name | dcterms.format | meta[title='gcformat'] |
      | /find-people-and-collaborate | name | dcterms.language | meta[title='ISO639-2'] |
      | /find-people-and-collaborate | content | fra | meta[title='ISO639-2'] |
      | /branches-and-offices | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /branches-and-offices | title | W3CDTF | meta[name='dcterms.issued'] |
      | /branches-and-offices | name | dcterms.format | meta[title='gcformat'] |
      | /branches-and-offices | name | dcterms.language | meta[title='ISO639-2'] |
      | /branches-and-offices | content | fra | meta[title='ISO639-2'] |
      | /human-resources | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /human-resources | title | W3CDTF | meta[name='dcterms.issued'] |
      | /human-resources | name | dcterms.format | meta[title='gcformat'] |
      | /human-resources | name | dcterms.language | meta[title='ISO639-2'] |
      | /human-resources | content | fra | meta[title='ISO639-2'] |
      | /our-department | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /our-department | title | W3CDTF | meta[name='dcterms.issued'] |
      | /our-department | name | dcterms.format | meta[title='gcformat'] |
      | /our-department | name | dcterms.language | meta[title='ISO639-2'] |
      | /our-department | content | fra | meta[title='ISO639-2'] |
      | /our-department/social-media-resources | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /our-department/social-media-resources | title | W3CDTF | meta[name='dcterms.issued'] |
      | /our-department/social-media-resources | name | dcterms.format | meta[title='gcformat'] |
      | /our-department/social-media-resources | name | dcterms.language | meta[title='ISO639-2'] |
      | /our-department/social-media-resources | content | fra | meta[title='ISO639-2'] |
      | /contact-us | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /contact-us | title | W3CDTF | meta[name='dcterms.issued'] |
      | /contact-us | name | dcterms.format | meta[title='gcformat'] |
      | /contact-us | name | dcterms.language | meta[title='ISO639-2'] |
      | /contact-us | content | fra | meta[title='ISO639-2'] |
      | /home/tools-and-services | content | Agriculture et Agroalimentaire Canada | meta[name='dcterms.creator'] |
      | /home/tools-and-services | title | W3CDTF | meta[name='dcterms.issued'] |
      | /home/tools-and-services | name | dcterms.format | meta[title='gcformat'] |
      | /home/tools-and-services | name | dcterms.language | meta[title='ISO639-2'] |
      | /home/tools-and-services | content | fra | meta[title='ISO639-2'] |

