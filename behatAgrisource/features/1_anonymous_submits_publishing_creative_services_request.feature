#/branches-and-offices/public-affairs-branch/publishing-and-creative-services-request-form
Feature: Anonymous submits a Publishing and Creative Services Request

  Scenario: Anonymous user must be able to submit aa Publishing and Creative Services Request in both EN and FR and the form validation should work on each field.
  # Verify that we are on the ENGLISH Publishing and Creative Services Request form and that an empty form submission generates validation errors
    Given I am on "/branches-and-offices/public-affairs-branch/publishing-and-creative-services-request-form"
    Then I should see "Publishing and Creative Services Request Form" in the "h1#wb-cont" element
    When I click the "#edit-actions-submit" button
    And I pause 5 seconds
    Then I should see "Name - This field is required."
    And I should see "Email - This field is required."
    And I should see "Branch - This field is required."
    And I should see "Is this a new request or an update to an existing project? - This field is required."
    And I should see "Project Title - This field is required."
    And I should see "Date Required - This field is required."
    And I should see "Is the request tied to a scheduled event? - This field is required."
    And I should see "Please identify which departmental priority your publishing request supports - This field is required."
    And I should see "What is the main message you wish to communicate? - This field is required."
    And I should see "Target Audience - This field is required."
    And I should see "Has your Communications Advisor approved this project? - This field is required."
    And I should see "Product - This field is required."
    And I should see "Has the content been approved - This field is required."    
    And I should see "Has the content been finalized in both English and French? - This field is required."
# And I should not see "Error message"

    # Add input or make selections for each mandatory field
    Then I fill in the following:
      | edit-srf-name | EN name goes here |
      | edit-srf-phone | +1 514-222-2222 |
      | edit-srf-email | agsbehat-publishingCreativeServicesRequest-tester@yopmail.com |

    Then I select option "Science and Technology Branch" from "edit-srf-branch"
    Then I select option "New" from "edit-srf-isnewrequestorexistingproject"
    Then I select option "No" from "edit-is-your-project-submission-related-to-any-other-request-already-"
    
    Then I fill in the following:
      | edit-rsf-projecttitle | EN Project Title goes here |
      | edit-rsf-daterequired | 2029-01-01 |

    Then I select option "No" from "edit-srf-isrequesttoscheduledevent"
    Then I select option "No" from "edit-srf-contentfinalizedinenglishandfrench"

    Then I fill in the following:
      | edit-srf-departmentalpriority | EN departmental priority goes here |
      | edit-srf-mainmessage | EN main message goes here |

    Then I select option "Consumers" from "edit-srf-targetaudience"
    Then I select option "No" from "edit-srf-hascommadvisorapproved"
#    Then I select option "Banner Stand" from "edit-srf-product"
    Then I select option "DG" from "edit-level-of-approval-required"
    And I check "Banner Stand"
    Then I select option "No" from "edit-srf-hascontentpproved"
    # last one deleted to prevent submitting the form
    #############Then I select option "No" from "edit-srf-contentfinalizedinenglishandfrench"

  # Verify each validation error is now gone
    Then I click the "#edit-actions-submit" button
    And I pause 3 seconds
    Then I should not see "Name - This field is required."
    And I should not see "Email - This field is required."
    And I should not see "Branch - This field is required."
    And I should not see "Is this a new request or an update to an existing project? - This field is required."
    And I should not see "Project Title - This field is required."
    And I should not see "Date Required - This field is required."
    And I should not see "Is the request tied to a scheduled event? - This field is required."
    And I should not see "Please identify which departmental priority your publishing request supports - This field is required."
    And I should not see "What is the main message you wish to communicate? - This field is required."
    And I should not see "Target Audience - This field is required."
    And I should not see "Has your Communications Advisor approved this project? - This field is required."
    And I should not see "Product - This field is required."
    And I should not see "Has the content been approved - This field is required."
    #Error shows up if email not configured correctly.
    #And I should not see "Error message"
    # see line 51 - uncomment both (line 52 & line line 71) at the end
    ############# And I should not see "Has the content been finalized in both English and French? field is required."

  # TODO - Only mandatory fields are validated, non-mandatory fields and not tested currently
    # Network User Name
    # Additional Comments


  # Skipped for now as we dont want to submit the form
  # Verify functionality of the button ... Submit
    #When I click the "#edit-actions-submit" button

  # Successful Submit should display a Confirmation message
    #And I should see "a été créé"
    And I should not see "errors have been found"
    And I should see "Request confirmed"
