Feature: log in and test embed block

  Scenario: Verify that embed block functionality is visible
    Given I am on "/user/logout"
    And I pause 3 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    And I should not see "Error message"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button
    And I pause 4 seconds
    Given I navigate to "/node/add/page"
    And I pause 3 seconds
    And I click the "a.cke_button__embedblock" button
    And I should see "Embed Block" in the "span.ui-dialog-title" element
