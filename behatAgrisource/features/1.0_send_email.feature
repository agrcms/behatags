Feature: Log in with admin, make sure that agrisource can send email.

  Scenario: Log in ( admin ), and test email functionality.

# First logout of the system, then login using admin level account
    Given I am on "/user/logout"
    And I pause 2 seconds

    Given I am on "/user/login"
    Then I should see "Log in"
    And I should not see "Error message"
    When I fill in "edit-name" with "agrisourcebehat_adm"
    And I fill in "edit-pass" with "Password1"
    And I click the "#edit-submit" button
    And I pause 4 seconds
    
    Given I am on "/admin/config/system/symfony-mailer-lite/test"
    And I fill in "#edit-recipient" with "j@7pro.ca"
    And I click the "#edit-actions-submit" button
  #  Then I pause 10 seconds
  #  Then I should not see "Unable to send email"
