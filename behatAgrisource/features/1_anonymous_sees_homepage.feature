Feature: Anonymous sees Homepage

  Scenario: Anonymous user must see the Homepage correctly in both EN and FR
  # Verify that on the EN homepage we do see the expected elements
    Given I navigate to "/"
    Then I should see "AgriSource" in the "a.navbar-brand" element
    And I should see "Find a colleague" in the "#block-findacolleague" element
    And I should see "media" in the "#block-aafcsocialmedia" element
    And I should see "news@work" in the "#block-newswork" element
    And I should see "Services and information" in the "#block-servicesandinformation" element
    And I should see "Features" in the "#block-features" element
    And I should see "Date modified" in the "#block-grstheme-bootstrap-datemodifiedblock" element
    And I should see "Collaborate" in the "nav#block-grstheme-bootstrap-footer" element
    And I should see "Important Notices" in the "ul#gc-tctr" element
    And I should see "Pause" in the "a.plypause" element
    And I should see "Search" in the "button#wb-srch-sub" element
    And I should not see "Error message"

  # Verify language switcher leads from EN to FR homepage
    Then I pause 1 seconds
    Then I click the "a.language-link" button

  # Verify that on the FR homepage we do see the expected elements
    Then I should see "AgriSource" in the "a.navbar-brand" element
    And I should see "Trouver un collègue" in the "#block-findacolleague" element
    And I should see "Suivez" in the "#block-aafcsocialmedia" element
    And I should see "nouvelles@l'ouvrage" in the "#block-newswork" element
    And I should see "Services et renseignements" in the "#block-servicesandinformation" element
    And I should see "En vedette" in the "#block-features" element
    And I should see "Date de modification" in the "#block-grstheme-bootstrap-datemodifiedblock" element
    And I should see "Collaborer" in the "nav#block-footer-fr" element
    And I should see "Avis importants" in the "ul#gc-tctr" element
    And I should see "Pause" in the "a.plypause" element
    And I should see "Recherche" in the "button#wb-srch-sub" element
    And I should not see "Message d'erreur"

  # Verify language switcher leads from FR back to EN homepage
    Then I pause 1 seconds
    Then I click the "a.language-link" button
    And I should see "Find a colleague" in the "#block-findacolleague" element

