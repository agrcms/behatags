Fonctionnalité: Page d'accueil
        Afin de confirmer que le site répond correctement
        Comme utilisateur du site web
        Je doit être capable de voir le titre du site et le lien Se connecter

        Scénario: Verifier la page accueil (en anglais)
        Soit je visite "/"
        Alors je devrait être sur "/"
		Alors I should see "Your Donor Portal" in the "h1" element
		Alors Francais
        Soit je visite "/"
		Alors I should see "Le don de sang"
		Et I click "Connexion"
		Alors je devrait être sur "/utilisateur/connexion"
		Et I click "English"
		Alors English
