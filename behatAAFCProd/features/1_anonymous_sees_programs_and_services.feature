Feature: Presence of Agricultural programs and services

  Scenario: Verify that Agricultural programs and services search and sorting is in place
    Given I navigate to "/programs"
    Then I should see "Agricultural programs and services" in the "h1#wb-cont" element
    Then I should see "Search all programs and services" in the "h2#lst" element
    And I should see "Showing 1 to " in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Programmes et services agricoles" in the "h1#wb-cont" element
    Then I should see "Trouver un programme ou un service" in the "h2#lst" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
