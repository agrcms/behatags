Feature: Presence of Agriculture and Agri-Food Canada’s Biological Collections feature

  Scenario: Verify that Research projects search is in place
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/aef/main/index_en.html?JOURNAL-EN=f736b79f4b6f4afb83d3a8f639689d05&JOURNAL-FR=929b1695c1744bb194006ad6e3eafe20&breadcrumb=can%2Cagr%2Cinnovation%2Cresearch&iframeheight=800px&fluid=true"
    Then I should see "Agriculture and Agri-Food Canada’s Biological Collections" in the "h1#mapTitle" element
    And I should see a "iframe" element
    And I should not see "Error message"
#    When I run the test in French
#    When I click the "a.language-link" button
#    LANGUAGE SWITCHER NOT WORKING
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/aef/main/index_fr.html?JOURNAL-EN=f736b79f4b6f4afb83d3a8f639689d05&JOURNAL-FR=929b1695c1744bb194006ad6e3eafe20&breadcrumb=can%2Cagr%2Cinnovation%2Cresearch&iframeheight=800px&fluid=true"
    And I pause 2 seconds
    Then I should see "Collections biologiques d'agriculture et agroalimentaire Canada" in the "h1#mapTitle" element
    And I should see a "iframe" element
    And I should not see "Message d'erreur"
