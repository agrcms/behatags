
Feature: Presence of Integrated pest management projects search

  Scenario: Verify that Integrated pest management projects search is in place
    Given I navigate to "/science/agriculture-and-agri-food-research-centres/pest-management-centre/pesticide-risk-reduction-pest-management-centre/integrated-pest-management-projects"
    Then I should see "Integrated pest management projects" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Projets de lutte intégrée" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
