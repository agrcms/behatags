Feature: Presence of Drought features

  Scenario: Verify that Drought Monitor is in place
    Given I navigate to "/agricultural-production/weather/canadian-drought-monitor"
    Then I should see "Canadian Drought Monitor" in the "h1#wb-cont" element
    And I should see "Drought conditions as of" in the "h2#drou" element
    And I should see a "iframe" element
    And I should not see "Error message"
    When I click the "a.language-link" button
    And I pause 4 seconds
    Then I should see "Outil de surveillance des sécheresses au Canada" in the "h1#wb-cont" element
    And I should see "Conditions de sécheresse au" in the "h2#con" element
    And I should see a "iframe" element
    And I should not see "Message d'erreur"

  Scenario: Verify that Drought Outlook is in place
    Given I navigate to "/agricultural-production/weather/canadian-drought-outlook"
    And I pause 4 seconds
    Then I should see "Canadian Drought Outlook" in the "h1#wb-cont" element
    And I should see a "iframe" element
    And I should not see "Error message"
    When I click the "a.language-link" button
    And I pause 4 seconds
    Then I should see "Aperçu de la sécheresse au Canada" in the "h1#wb-cont" element
    And I should see a "iframe" element
    And I should not see "Error message"
