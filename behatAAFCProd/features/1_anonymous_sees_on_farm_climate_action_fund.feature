Feature: Presence of On-Farm Climate Action Fund recipient organization

  Scenario: Verify that Search for more Good News Grows stories is in place
    Given I navigate to "/programs/agricultural-climate-solutions-farm-climate-action-fund"
    Then I should see "Agricultural Climate Solutions – On-Farm Climate Action Fund" in the "h1#wb-cont" element
    Then I should see "Intake period" in the "section.alert h3" element
    And I should see "Find an On-Farm Climate Action Fund recipient organization" in the "h3#find" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Solutions agricoles pour le climat – Fonds d'action à la ferme pour le climat" in the "h1#wb-cont" element
    Then I should see "Période de réception des demandes" in the "section.alert h3" element
    And I should see "Trouver une organisation bénéficiaire du Fonds d’action à la ferme pour le climat" in the "h3#find" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
