Feature: Presence of Minor Use Pesticides search

  Scenario: Verify that Minor Use Pesticides buildings search is in place
    Given I navigate to "/science/agriculture-and-agri-food-research-centres/pest-management-centre/minor-use-pesticides-pest-management-centre/minor-use-pesticides/list-submissions-minor-use-pesticides-small-acreage-crops"
    Then I should see "List of submissions of minor use pesticides on small-acreage crops" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Liste des demandes de pesticides à usage limité déposées pour des cultures sur surfaces réduites" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
