Feature: Webform testing - Submit your feedback - Programs and Services

  Scenario: Verify that weform is functioning Submit your feedback Programs and Services
    Given I navigate to "/agricultural-programs-and-services/submit-your-feedback-programs-and-services"
    Then I should see "Submit your feedback - Programs and Services" in the "#wb-cont" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Commentaires sur les programmes et les services" in the "#wb-cont" element
    And I should not see "Message d'erreur"
