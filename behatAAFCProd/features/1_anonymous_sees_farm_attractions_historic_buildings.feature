Feature: Presence of Farm Attractions Historical buildings search

  Scenario: Verify that Farm Attractions Historical buildings search is in place
    Given I navigate to "/contact/central-experimental-farm/farm-attractions"
    Then I should see "Farm Attractions" in the "h1#wb-cont" element
    And I should see "Historical buildings" in the "h3#f" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Points d’intérêt de la Ferme" in the "h1#wb-cont" element
    And I should see "Bâtiments historiques" in the "h3#f" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
