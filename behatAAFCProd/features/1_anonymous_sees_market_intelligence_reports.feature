Feature: Presence of Market intelligence reports

  Scenario: Verify that Integrated pest management projects search is in place
    Given I navigate to "/international-trade/market-intelligence/reports"
    Then I should see "Market intelligence reports" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#products-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Rapports d'information sur les marchés" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#products-filter_info" element
    And I should not see "Message d'erreur"
