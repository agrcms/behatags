Feature: Presence of job search - Step up to the plate

  Scenario: Verify that search block for job websites and employee resources is in place
    Given I navigate to "/sector/step-plate"
    Then I should see "Step up to the plate" in the "h1#wb-cont" element
    And I should see "Showing 1 to 10 of" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Mettez la main à la pâte" in the "h1#wb-cont" element
    And I should see "Affiche 1 à 10 de" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"

Scenario: Verify that search block for Step up to the plate promotional resources in place
    Given I navigate to "/sector/step-plate/promotional-resources"
    Then I should see "Step up to the plate promotional resources" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#wb-auto-4_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Ressources promotionnelles – Mettez la main à la pâte" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#wb-auto-4_info" element
    And I should not see "Message d'erreur"
