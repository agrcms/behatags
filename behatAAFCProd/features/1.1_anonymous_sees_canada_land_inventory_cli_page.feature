Feature: Presence of Canada Land Inventory CLI Page feature

  Scenario: Verify that Canada Land Inventory CLI Page is in place
   Given I navigate to "https://sis.agr.gc.ca/cansis/nsdb/cli/index.html"
    And I pause 6 seconds
    Then I should see "Canada Land Inventory (CLI)" in the "h1" element
    And I should see "Showing 1 to" in the "#wb-auto-4_info" element
    And I should not see "Error message"

   Given I navigate to "https://sis.agr.gc.ca/siscan/nsdb/cli/index.html"
    And I pause 6 seconds
    Then I should see "Index de l'Inventaire des terres du Canada (ITC)" in the "h1" element
    And I should see "Affiche 1 à" in the "#wb-auto-4_info" element
    And I should not see "Message d'erreur"
