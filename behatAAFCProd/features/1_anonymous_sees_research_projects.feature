Feature: Presence of Research projects search

  Scenario: Verify that Research projects search is in place
    Given I navigate to "/science/research-projects"
    Then I should see "Research projects" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Projets de recherche" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
