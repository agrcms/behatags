Feature: Presence of Extreme Weather Indices iframe maps

  Scenario: Verify that Extreme Weather Indices is in place
    Given I navigate to "/agricultural-production/weather/extreme-weather-indices"
    Then I should see "Extreme Weather Indices" in the "h1#wb-cont" element
    And I should see a "iframe#ewi-iframe" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 4 seconds
    Then I should see "Indices de conditions météorologiques extrêmes" in the "h1#wb-cont" element
    And I should see a "iframe#ewi-iframe" element
    And I should not see "Message d'erreur"
