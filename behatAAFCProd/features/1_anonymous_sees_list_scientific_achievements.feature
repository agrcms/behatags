Feature: Presence of List of scientific achievements in agriculture search

  Scenario: Verify that List of scientific achievements in agriculture search is in place
    Given I navigate to "/science/story-agricultural-science/scientific-achievements-agriculture"
    Then I should see "Scientific achievements in agriculture" in the "h1#wb-cont" element
#    And I should see "List of scientific achievements in agriculture" in the "h2" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Réalisations scientifiques en agriculture" in the "h1#wb-cont" element
#    And I should see "Liste complète des réalisations scientifiques en agriculture" in the "h2" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
