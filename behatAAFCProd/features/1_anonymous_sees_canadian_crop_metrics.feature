Feature: Presence of Canadian Crop Metrics iframe maps

  Scenario: Verify that Canadian Crop Metrics is in place
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/metrics/index-en.html?appid=ccm-epc"
    Then I should see "Canadian Crop Metrics" in the "h1#wb-cont" element
    And I should see a "details#panel_mapoptions" element
    And I should see a "div#viewDiv" element
    And I should not see "Error message"
#    When I run the test in French
#    When I click the "a.language-link" button
# LANGUAGE SWITCHER NOT WORKING
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/metrics/index-fr.html?appid=ccm-epc"
    And I pause 4 seconds
    Then I should see "Évaluation de la productivité des cultures" in the "h1#wb-cont" element
    And I should see a "details#panel_mapoptions" element
    And I should see a "div#viewDiv" element
    And I should not see "Message d'erreur"
