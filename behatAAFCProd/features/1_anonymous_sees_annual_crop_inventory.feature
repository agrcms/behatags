Feature: Presence of Annual crop inventory search

  Scenario: Verify that Annual crop inventory search is in place
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/metrics/index-en.html?appid=aci-iac"
    Then I should see "Annual crop inventory" in the "h1#wb-cont" element
    And I should see a "canvas" element
    And I should not see "Error message"
#    When I run the test in French
#    When I click the "a.language-link" button
# language switcher not working
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/metrics/index-fr.html?appid=aci-iac"
    And I pause 2 seconds
    Then I should see "Inventaire annuel des cultures" in the "h1#wb-cont" element
    And I should see a "canvas" element
    And I should not see "Message d'erreur"
