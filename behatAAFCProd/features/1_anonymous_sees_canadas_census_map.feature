Feature: Presence of Canada's Census of Agriculture map feature

  Scenario: Verify that Canada's Census of Agriculture map is in place
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/aef/main/index_en.html?AGRIAPP=15"
    And I pause 6 seconds
    Then I should see "Canada's Census of Agriculture" in the "h1#mapTitle" element
    And I should see a "iframe" element
    And I should not see "Error message"
#    When I run the test in French
#    When I click the "a.language-link" button
#    LANGUAGE SWITCHER NOT WORKING
    Given I navigate to "https://agriculture.canada.ca/atlas/apps/aef/main/index_fr.html?AGRIAPP=15"
    And I pause 6 seconds
    Then I should see "Recensement de l'agriculture du Canada" in the "h1#mapTitle" element
    And I should see a "iframe" element
    And I should not see "Message d'erreur"
