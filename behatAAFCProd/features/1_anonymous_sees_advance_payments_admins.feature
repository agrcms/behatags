Feature: Presence of Advance Payments Program: Advance Payments Program Administrators search

  Scenario: Verify that Advance Payments Program: Advance Payments Program Administrators search is in place
    Given I navigate to "/programs/advance-payments/program-administrators"
    Then I should see "Advance Payments Program: Advance Payments Program Administrators" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Programme de paiements anticipés : Agents d’exécution du Programme de paiements anticipés" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
