Feature: Presence of Audits and Evaluations search

  Scenario: Verify that Audits and Evaluations search is in place
    Given I navigate to "/department/transparency/audits-evaluations"
    Then I should see "Audits and evaluations" in the "h1#wb-cont" element
    And I should see "Showing 1 to" in the "div#dataset-filter_info" element
    And I should not see "Error message"
#    When I run the test in French
    When I click the "a.language-link" button
    And I pause 2 seconds
    Then I should see "Audits et évaluations" in the "h1#wb-cont" element
    And I should see "Affiche 1 à" in the "div#dataset-filter_info" element
    And I should not see "Message d'erreur"
