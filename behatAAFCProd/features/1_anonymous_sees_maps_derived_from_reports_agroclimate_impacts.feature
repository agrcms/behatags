Feature: Presence of Maps derived from reports on agroclimate impacts

  Scenario: Verify that Maps derived from reports on agroclimate impacts maps are in place
    Given I navigate to "https://www.agr.gc.ca/DW-GS/mapselector-selecteurdecartes.jspx?lang=eng&jsEnabled=true"
    Then I should see "Maps derived from reports on agroclimate impacts" in the "h1#wb-cont" element
    And I should see a "img#imgMap" element
    And I should not see "Error message"
#    When I run the test in French
#    When I click the "a.language-link" button
# Language switcher not working
     Given I navigate to "https://www.agr.gc.ca/DW-GS/mapselector-selecteurdecartes.jspx?lang=fra&jsEnabled=true"   
    And I pause 2 seconds
    Then I should see "Rapports sur les impacts agroclimatiques" in the "h1#wb-cont" element
    And I should see a "img#imgMap" element
    And I should not see "Message d'erreur"
